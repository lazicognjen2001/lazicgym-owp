drop schema if exists gym;
create schema gym default character set utf8;

create table korisnici(
	id int primary key,
    korisnickoIme varchar(50) unique,
    lozinka varchar(50) not null,
    email varchar(50) not null,
    ime varchar(20) not null,
    prezime varchar(20) not null,
    datumRodjenja date not null,
    adresa varchar(50) not null,
    brojTelefona bigint not null,
    datumRegistracije datetime not null,
    tipKorisnika enum('clan','admin'),
    blokiran bool not null,
    aktivan bool not null
);
insert into korisnici values(1,'ognjen','ognjen','ognjen@gmail.com','Ognjen','Lazic','2001-03-12','Rudo Polje 102',066545537,'2021-12-03 10:42:00','clan',false,true);
insert into korisnici values(2,'vlada','vlada','vlada@gmail.com','Vladimir','Ðurðevic','2001-11-27','Branka Æopiæa',0642242451,'2021-12-12 09:21:00','clan',false,true);
insert into korisnici values(3,'milovan','milovan','milovan@gmail.com','Milovan','Jaæimoviæ','2000-10-04','Srpskih Ustanika',0645678934,'2021-12-30 06:10:00','admin',false,true);
insert into korisnici values(4,'petar','petar','petar@gmail.com','Petar','Petrovic','2001-08-05','Petra Koèiæa',0665789098,'2020-12-24 15:36:00','admin',false,true);

create table clanskaKarta(
	id int primary key,
    popust int not null,
    brojPoena int not null,
    clanID int,
    foreign key(clanID) REFERENCES korisnici(id),
    prihvacen bool not null,
    iskoricena bool not null,
    aktivan bool not null
);
insert into clanskaKarta values(1,10,6,1,true,false,true);
insert into clanskaKarta values(2,5,1,2,true,true,true);

create table tipTreninga(
	id int primary key,
    ime varchar(50) not null,
    opis varchar(500) not null,
    aktivan bool not null
);
insert into tipTreninga values(1,'Kardio','Kardio trenig pomijera granice vaše izdržljivosti. Sastoji se od treninga na traci za trèanje, biciklu kao i promjenljivih kretanja na poligonu.',true);
insert into tipTreninga values(2,'Joga','Trning joge predstavlja novi tip treninga u našoj teretani. Sastoji se od treninga sa profesionalnim trenerom uz pomoæ rekvizita za lakše izvoðenje vježbi joge.',true);
insert into tipTreninga values(3,'Trening snage','Trening snage se sastoji od vježbi koje ukljuèuju rad na razlièitim spravama za razvoj ruènih, grudnih, nožnih ali i mišića abdomena. Izvodi se i na poligonu za workout.',true);
insert into tipTreninga values(4,'Kružni trening','Kružni trening je trening koji na najbolji naèin testira vašu izdržljivost i kondiciju. Sastoji se od nekoliko serija kardio vježbi i vježbi snage.',true);

create table trening(
	id int primary key,
    naziv varchar(50) not null,
    trener varchar(50) not null,
    opis varchar(300) not null,
    tipTreningaID int,
    foreign key (tipTreningaID) references tipTreninga(id),
    cijena float not null,
    vrstaTreninga enum('grupni','pojedinacni'),
    nivoTreninga enum('amaterski','srednji','napredni'),
    slika varchar(300) not null,
    trajanjeTreninga int not null,
    prosjecnaOcena float not null,
    aktivan bool not null
);
insert into trening values(1,'Sportski trening','Milinko Savic','Trening za sve profesionalne sportiste, i one koji se tako osjecaju',4,2500,'grupni','srednji','https://images.unsplash.com/photo-1599058917212-d750089bc07e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1469&q=80',60,4.0,true);
insert into trening values(2,'Trening izdržljivosti','Radovan Peric','Trening koji testira vaše granice, i postavlja nove.',3,2000,'pojedinacni','napredni','https://images.unsplash.com/photo-1594381898411-846e7d193883?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80',90,5.0,true);
insert into trening values(3,'Trening za ruke','Mihajlo Mitrovic','Trening fokusiran na ruke i ramena.',3,1500,'pojedinacni','amaterski','https://images.unsplash.com/photo-1517964603305-11c0f6f66012?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1471&q=80',35,3.9,true);
insert into trening values(4,'Vojni trening','Bela Mihalj','Trening baziran na pokretima, kao i snalaženju polaznika u pokretu sa razlièitim preprekama',1,2000,'grupni','napredni','https://images.unsplash.com/photo-1605296867304-46d5465a13f1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80',90,4.1,true);

create table tipoviTreningaLista(
	id int primary key,
    treningID int,
    foreign key (treningID) references trening(id),
    tipTreningaID int,
    foreign key (tipTreningaID) references tipTreninga(id),
    aktivan boolean
);

insert into tipoviTreningaLista values (1,1,2,true);
insert into tipoviTreningaLista values (2,1,3,true);
insert into tipoviTreningaLista values (3,2,1,true);
insert into tipoviTreningaLista values (4,2,2,true);
insert into tipoviTreningaLista values (5,3,2,true);
insert into tipoviTreningaLista values (6,4,4,true);

create table komentar(
	id int primary key,
    tekstKomentara varchar(500),
    ocjena float,
    datumPostavljanja datetime,
    clanID int,
    foreign key(clanID) references korisnici(id),
    treningID int,
    foreign key(treningID) references trening(id),
    statusKomentara enum('naCekanju','odobren','odbijen'),
    anoniman bool not null,
    aktivan bool not null
);
insert into komentar values(1,'Sve je bilo super!', 5.0,'2022-01-02 10:11:00',2,3,'odobren',false,true);
insert into komentar values(2,'Može to mnogo bolje', 3.0,'2022-01-02 09:22:00',1,2,'odobren',false,true);
insert into komentar values(3,'OK je',4.0,'2022-01-03 12:00:00',2,1,'odobren',false,true);
insert into komentar values(4,'Predobar trening, preporuke',5.0,'2022-01-04 17:20:00',null,4,'naCekanju',true,true);

create table korpa(
	id int primary key,
    nazivTreninga varchar(50) not null,
    trener varchar(50) not null,
    tipTreningaID int,
    foreign key (tipTreningaID) references tipTreninga(id),
    datumOdrzavanja datetime not null,
    cena float not null,
    aktivan bool not null
);
insert into korpa values(1,'Kardio','Slavko Mitrovic',3,'2022-01-22 11:20:00',2500,true);
insert into korpa values(2,'Trening za ruke','Mirko Memedovic',2,'2022-01-27 12:00:00',2600,true);
insert into korpa values(3,'Vojni trening','Predrag Rajkovic',1,'2022-01-17 13:00:00',3400,true);
insert into korpa values(4,'Sportski trening','Slavko Mitrovic',4,'2022-01-20 14:00:00',3000,true);

create table listaZelja(
	id int primary key,
    clanID int,
    foreign key (clanID) references korisnici(id),
    treningID int,
    foreign key (treningID) references trening(id),
    aktivan bool not null
);
insert into listaZelja values(1,2,3,true);
insert into listaZelja values(2,3,2,true);
insert into listaZelja values(3,4,4,true);
insert into listaZelja values(4,1,1,true);

create table sala(
	id int primary key,
    oznakaSale varchar(50) not null,
    kapacitet int not null,
    popunjen bool not null,
    aktivan bool not null
);
insert into sala values(1,'Kardio',60,true,true);
insert into sala values(2,'Sala Novak Djokovic',10,true,true);
insert into sala values(3,'Fitness sala',30,false,true);
insert into sala values(4,'Yoga Sala',7,false,true);

create table specijalniDatum(
	id int primary key,
    datum datetime not null,
    popust int not null,
    aktivan bool not null
);
insert into specijalniDatum values(1,'2022-01-07 09:00:00',50,true);
insert into specijalniDatum values(2,'2022-01-20 11:00:00',50,true);
insert into specijalniDatum values(3,'2022-02-25 14:00:00',30,true);
insert into specijalniDatum values(4,'2022-02-20 08:00:00',20,true);

create table terminTreninga(
	id int primary key,
    salaID int,
    foreign key (salaID) references sala(id),
    treningID int,
    foreign key (treningID) references trening(id),
    clanID int,
    foreign key (clanID) references korisnici(id),
    datumOdrzavanja datetime not null,
    aktivan bool not null,
    popunjen bool not null
);
insert into terminTreninga values(1,3,2,1,'2022-01-25 13:00:00',true,false);
insert into terminTreninga values(2,4,3,2,'2022-02-10 09:00:00',true,false);
insert into terminTreninga values(3,2,4,1,'2022-01-21 10:00:00',true,false);
insert into terminTreninga values(4,1,1,2,'2022-02-02 18:00:00',true,false);

select * from komentar;
select * from clanskaKarta;
select * from korisnici;
select * from korpa;
select * from listaZelja;
select * from sala;
select * from specijalnidatum;
select * from termintreninga;
select * from tiptreninga;
select * from trening;