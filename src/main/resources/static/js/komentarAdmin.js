function json(response) {
  return response.json()
}

fetch('komentar/sviKomentariAdmin', {
    method: 'get'
  }).then(response => {
	  return response.text()
  }).then((data) => {
	  console.log(data ? JSON.parse(data) : {})
	  listaKomentara = JSON.parse(data)
	  
	  var table = document.getElementById("komentari");
	  for (var i=0; i < listaKomentara.length ; i++){
	       var tr = document.createElement('TR');
	       table.appendChild(tr);
	       
	       var tdId = document.createElement('TD');
	       tdId.appendChild(document.createTextNode(listaKomentara[i].id));
	       tr.appendChild(tdId);
	       
	       var tdKomentar = document.createElement('TD');
	       tdKomentar.appendChild(document.createTextNode(listaKomentara[i].tekst));
	       tr.appendChild(tdKomentar);
	       
	       var tdOcjena = document.createElement('TD');
	       tdOcjena.appendChild(document.createTextNode(listaKomentara[i].ocjena));
	       tr.appendChild(tdOcjena);
	       
	       var tdDatumPostavljanja = document.createElement('TD');
	       tdDatumPostavljanja.appendChild(document.createTextNode(listaKomentara[i].datumPostavljanja));
	       tr.appendChild(tdDatumPostavljanja);

			
	       
	       var tdFormaPrihvati = document.createElement('TD')
	       var formPrihvati = document.createElement('FORM')
	       
	       formPrihvati.setAttribute('method', "get")
	       formPrihvati.setAttribute('action', "komentar/prihvati")
	       
	       var inputHiddenPrihvati = document.createElement('INPUT')
	       
	       inputHiddenPrihvati.setAttribute('type', "hidden")
	       inputHiddenPrihvati.setAttribute('name', "id")
	       inputHiddenPrihvati.setAttribute('value', listaKomentara[i].id)
	       
	       var inputSubmitPrihvati = document.createElement('INPUT')
	       inputSubmitPrihvati.setAttribute('type', "submit")
	       inputSubmitPrihvati.setAttribute('value', "Prihvati")
	       
	       formPrihvati.appendChild(inputHiddenPrihvati)
	       formPrihvati.appendChild(inputSubmitPrihvati)
	       
	       tdFormaPrihvati.appendChild(formPrihvati)
	       
	       tr.appendChild(tdFormaPrihvati);

			var tdFormaOdbij = document.createElement('TD')
	       var formOdbij = document.createElement('FORM')
	       
	       formOdbij.setAttribute('method', "get")
	       formOdbij.setAttribute('action', "komentar/odbij")
	       
	       var inputHiddenOdbij = document.createElement('INPUT')
	       
	       inputHiddenOdbij.setAttribute('type', "hidden")
	       inputHiddenOdbij.setAttribute('name', "id")
	       inputHiddenOdbij.setAttribute('value', listaKomentara[i].id)
	       
	       var inputSubmitOdbij = document.createElement('INPUT')
	       inputSubmitOdbij.setAttribute('type', "submit")
	       inputSubmitOdbij.setAttribute('value', "Odbij")
	       
	       formOdbij.appendChild(inputHiddenOdbij)
	       formOdbij.appendChild(inputSubmitOdbij)
	       
	       tdFormaOdbij.appendChild(formOdbij)
	       
	       tr.appendChild(tdFormaOdbij);
			
	    }
	})
  .catch(function (error) {
    console.log('Request failed', error);
  });