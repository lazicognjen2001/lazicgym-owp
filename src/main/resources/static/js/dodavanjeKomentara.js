$(document).ready(function() {

	// keširanje referenci na elemente
	var treningId = $("input[name=treningId]")
	var komentar = $("input[name=komentar]")
	var ocjena = $("input[name=ocjena]")
	var anoniman = $("input[name=anoniman]")
	
	function dodaj() {
		// čitanje parametara forme za dodavanje
		var idTr = treningId.val()
		var kom = komentar.val()
		var ocj = ocjena.val()
		var anon = anoniman.val()
	
		// parametri zahteva
		var params = {
			treningId: idTr,
			komentar : kom,
			ocjena: ocj,
			anoniman : anon,
		}
		console.log(params)
		$.post("komentar/dodaj",
				params,
				function(){
					window.location.href = 'trening'
				}
		)
		console.log("POST: " + "knjige/add")
		
		return false // sprečiti da submit forme promeni stranicu
	}
	
	$("form").submit(dodaj) // registracija handler-a za dodavanje
})