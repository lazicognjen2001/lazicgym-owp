package com.ognjenlazic.LazicGym.model;

public class Sala {
	
	public Sala(
			String oznaka,
			int kapacitet,
			int id,
			boolean popunjen,
			boolean aktivan
			) {
		this.oznaka = oznaka;
		this.kapacitet = kapacitet;
		this.id = id;
		this.popunjen = popunjen;
		this.aktivan = aktivan;
	}
	
	public Sala() {
		
	}

	private String oznaka;
	private int kapacitet;
	private int id;
	private boolean popunjen;
	private boolean aktivan;
	
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}
	public String getOznaka() {
		return oznaka;
	}
	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean getAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}
	public boolean getPopunjen() {
		return popunjen;
	}
	public void setPopunjen(boolean popunjen) {
		this.popunjen = popunjen;
	} 
}
