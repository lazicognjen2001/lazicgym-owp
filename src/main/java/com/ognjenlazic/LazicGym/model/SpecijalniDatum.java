package com.ognjenlazic.LazicGym.model;

import java.time.LocalDateTime;

public class SpecijalniDatum {
	
	public SpecijalniDatum(
			int id,
			LocalDateTime datum,
			int popust,
			boolean aktivan
			) {
		this.id= id;
		this.datum = datum;
		this.popust = popust;
		this.aktivan = aktivan;
	}
	
	public SpecijalniDatum() {
		
	};
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getDatum() {
		return datum;
	}

	public void setDatum(LocalDateTime datum) {
		this.datum = datum;
	}

	public int getPopust() {
		return popust;
	}

	public void setPopust(int popust) {
		this.popust = popust;
	}

	public boolean isAktivan() {
		return aktivan;
	}

	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}

	private int id;
	private LocalDateTime datum;
	private int popust;
	private boolean aktivan;
}
