package com.ognjenlazic.LazicGym.model;

import java.time.LocalDateTime;

import com.ognjenlazic.LazicGym.model.enumeration.StatusKomentara;

public class Komentar {
	
	public Komentar(
			int id,
			String tekst,
			int ocjena,
			LocalDateTime datumPostavljanja,
			Korisnik korisnik,
			Trening trening,
			StatusKomentara status,
			 boolean anoniman,
			 boolean aktivan
			) {
		this.id = id;
		this.tekst = tekst;
		this.ocjena = ocjena;
		this.datumPostavljanja = datumPostavljanja;
		this.korisnik = korisnik;
		this.trening = trening;
		this.status = status;
		this.anoniman = anoniman;
		this.aktivan = aktivan;
	}
	
	public Komentar() {}
	
	private int id;
	private String tekst;
	private int ocjena;
	private LocalDateTime datumPostavljanja;
	private Korisnik korisnik;
	private Trening trening;
	private StatusKomentara status;
	private boolean anoniman;
	private boolean aktivan;
	
	public String getTekst() {
		return tekst;
	}
	public void setTekst(String tekst) {
		this.tekst = tekst;
	}
	public int getOcjena() {
		return ocjena;
	}
	public void setOcjena(int ocjena) {
		this.ocjena = ocjena;
	}
	public LocalDateTime getDatumPostavljanja() {
		return datumPostavljanja;
	}
	public void setDatumPostavljanja(LocalDateTime datumPostavljanja) {
		this.datumPostavljanja = datumPostavljanja;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public Trening getTrening() {
		return trening;
	}
	public void setTrening(Trening trening) {
		this.trening = trening;
	}
	public StatusKomentara getStatus() {
		return status;
	}
	public void setStatus(StatusKomentara status) {
		this.status = status;
	}
	public boolean isAnoniman() {
		return anoniman;
	}
	public void setAnoniman(boolean anoniman) {
		this.anoniman = anoniman;
	}
	public boolean isAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
