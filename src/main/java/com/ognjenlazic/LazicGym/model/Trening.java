package com.ognjenlazic.LazicGym.model;

import java.util.List;

import com.ognjenlazic.LazicGym.model.enumeration.NivoTreninga;
import com.ognjenlazic.LazicGym.model.enumeration.VrstaTreninga;

public class Trening {
	
	public Trening(int id,
			String naziv,
			String trener,
			String opis, 
			int cijena,
			VrstaTreninga vrstaTreninga,
			NivoTreninga nivoTreninga,
			TipTreninga tipTreninga,
			String urlSlike,
			int trajanje,
			double prosjecnaOcjena,
			boolean aktivan) {
		this.id = id;
		this.naziv = naziv;
		this.trener = trener;
		this.opis = opis;
		this.cijena = cijena;
		this.vrsta = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.slika = urlSlike;
		this.trajanjeTreninga = trajanje;
		this.prosjecnaOcjena = prosjecnaOcjena;
		this.aktivan = aktivan;
		this.tip = tipTreninga;
	}
	
	public Trening() {
		
	}
	
	private int id;
	private String naziv;
	private List<String> treneri;
	private String opis;
	private String slika;
	private TipTreninga tip; // Dobija se kod ucitavanja uz pomoc find one metode za trazeni id entiteta
	private int cijena;
	private VrstaTreninga vrsta;
	private NivoTreninga nivoTreninga;
	private int trajanjeTreninga;
	private double prosjecnaOcjena;
	private boolean aktivan;
	private String trener;
	
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public List<String> getTreneri() {
		return treneri;
	}
	public void setTreneri(List<String> treneri) {
		this.treneri = treneri;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	public int getCijena() {
		return cijena;
	}
	public void setCijena(int cijena) {
		this.cijena = cijena;
	}
	public VrstaTreninga getVrsta() {
		return vrsta;
	}
	public void setVrsta(VrstaTreninga vrsta) {
		this.vrsta = vrsta;
	}
	public TipTreninga getTip() {
		return tip;
	}
	public void setTip(TipTreninga tip) {
		this.tip = tip;
	}
	public NivoTreninga getNivoTreninga() {
		return nivoTreninga;
	}
	public void setNivoTreninga(NivoTreninga nivoTreninga) {
		this.nivoTreninga = nivoTreninga;
	}
	public int getTrajanjeTreninga() {
		return trajanjeTreninga;
	}
	public void setTrajanjeTreninga(int trajanjeTreninga) {
		this.trajanjeTreninga = trajanjeTreninga;
	}
	public double getProsjecnaOcjena() {
		return prosjecnaOcjena;
	}
	public void setProsjecnaOcjena(double prosjecnaOcjena) {
		this.prosjecnaOcjena = prosjecnaOcjena;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}
	public String getTrener() {
		return trener;
	}
	public void setTrener(String trener) {
		this.trener = trener;
	}

}
