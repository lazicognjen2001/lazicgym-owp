package com.ognjenlazic.LazicGym.model.enumeration;

public enum NivoTreninga {
	amaterski,
	srednji,
	napredni
}
