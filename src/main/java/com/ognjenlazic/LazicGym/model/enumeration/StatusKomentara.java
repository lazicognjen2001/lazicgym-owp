package com.ognjenlazic.LazicGym.model.enumeration;

public enum StatusKomentara {
	naCekanju,
	odobren,
	odbijen
}
