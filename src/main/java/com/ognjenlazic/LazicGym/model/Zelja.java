package com.ognjenlazic.LazicGym.model;

public class Zelja {
	
	public Zelja (
			int id,
			Korisnik korisnik,
			Trening trening,
			boolean aktivan
			) {
		this.id = id;
		this.korisnik = korisnik;
		this.trening = trening;
		this.aktivan = aktivan;
	}
	
	private int id;
	private Korisnik korisnik;
	private Trening trening;
	private boolean aktivan;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public Trening getTrening() {
		return trening;
	}
	public void setTrening(Trening trening) {
		this.trening = trening;
	}
	public boolean isAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}

}
