package com.ognjenlazic.LazicGym.model;

import java.time.Duration;
import java.time.LocalDateTime;

public class Termin {
	
	public Termin(
			int id,
			Sala sala,
			Trening trening,
			Korisnik korisnik,
			LocalDateTime datumIVrijemeTreninga,
			boolean popunjen,
			boolean aktivan
			) {
		this.id = id; 
		this.sala = sala;
		this.trening = trening;
		this.datumIVrijemeTreninga = datumIVrijemeTreninga;
		this.popunjen = popunjen;
		this.aktivan = aktivan;
		this.korisnik = korisnik;
		
		LocalDateTime now = LocalDateTime.now();  
		
		Duration duration = Duration.between(now, datumIVrijemeTreninga);
		System.out.println("DURATION: " + duration.toHours());
		if(duration.toHours() > 24) {
			this.manjeOd24h = true;
		} else {
			this.manjeOd24h = false;
		}
	}
	
	public Termin() {
		
	}
	
	private int id; 
	private Sala sala;
	private Trening trening;
	private LocalDateTime datumIVrijemeTreninga;
	private Korisnik korisnik;
	private boolean popunjen; 
	private boolean aktivan; 
	private boolean manjeOd24h;
	
	public Sala getSala() {
		return sala;
	}
	public void setSala(Sala sala) {
		this.sala = sala;
	}
	public LocalDateTime getDatumIVrijemeTreninga() {
		return datumIVrijemeTreninga;
	}
	public void setDatumIVrijemeTreninga(LocalDateTime datumIVrijemeTreninga) {
		this.datumIVrijemeTreninga = datumIVrijemeTreninga;
	}
	public Trening getTrening() {
		return trening;
	}
	public void setTrening(Trening trening) {
		this.trening = trening;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public boolean isPopunjen() {
		return popunjen;
	}
	public void setPopunjen(boolean popunjen) {
		this.popunjen = popunjen;
	}
	public boolean isAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isManjeOd24h() {
		return manjeOd24h;
	}

	public void setManjeOd24h(boolean manjeOd24h) {
		this.manjeOd24h = manjeOd24h;
	}

}
