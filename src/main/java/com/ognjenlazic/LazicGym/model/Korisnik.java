package com.ognjenlazic.LazicGym.model;

import java.time.LocalDateTime;
import java.util.Date;

import com.ognjenlazic.LazicGym.model.enumeration.Uloga;

public class Korisnik {
	
	public Korisnik (int id,
			String korisnickoIme, 
			String lozinka, 
			String email,
			String ime,
			String prezime, 
			Date datumRodjenja, 
			String adresa,
			long brojTelefona, 
			LocalDateTime datumIVrijemeRegistracije,
			Uloga uloga, 
			boolean blokiran,
			boolean aktivan) {
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.adresa = adresa;
		this.brojTelefona = brojTelefona;
		this.uloga = uloga;
		this.datumIVrijemeRegistracije =  datumIVrijemeRegistracije;
		this.blokiran = blokiran;
		this.aktivan = aktivan;
	}
	
	public Korisnik(String korisnickoIme, 
			String lozinka, 
			String email,
			String ime) {
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
		this.ime = ime;
	}
	
	private int id; 
	private String korisnickoIme;
	private String lozinka;
	private String email;
	private String ime;
	private String prezime;
	private Date datumRodjenja;
	private String adresa;
	private long brojTelefona;
	private Uloga uloga;
	private LocalDateTime datumIVrijemeRegistracije;
	private boolean blokiran;
	private boolean aktivan;
	
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public Date getDatumRodjenja() {
		return datumRodjenja;
	}
	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public long getBrojTelefona() {
		return brojTelefona;
	}
	public void setBrojTelefona(long brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	public Uloga getUloga() {
		return uloga;
	}
	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}
	public LocalDateTime getDatumIVrijemeRegistracije() {
		return datumIVrijemeRegistracije;
	}
	public void setDatumIVrijemeRegistracije(LocalDateTime datumIVrijemeRegistracije) {
		this.datumIVrijemeRegistracije = datumIVrijemeRegistracije;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isBlokiran() {
		return blokiran;
	}
	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}
	public boolean isAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}
	

}
