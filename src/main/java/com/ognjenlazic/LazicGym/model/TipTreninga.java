package com.ognjenlazic.LazicGym.model;

public class TipTreninga {
	
	public TipTreninga(
			String imeTreninga,
			String opis,
			int id,
			boolean aktivan
			) {
		this.id = id;
		this.imeTreninga = imeTreninga;
		this.opis = opis;
		this.aktivan = aktivan;
		}
	
	public TipTreninga( ) {
		
	}
	
	private String imeTreninga;
	private String opis;
	private int id;
	private boolean aktivan;
	
	public boolean getAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}
	
	public String getImeTreninga() {
		return imeTreninga;
	}
	public void setImeTreninga(String imeTreninga) {
		this.imeTreninga = imeTreninga;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}

