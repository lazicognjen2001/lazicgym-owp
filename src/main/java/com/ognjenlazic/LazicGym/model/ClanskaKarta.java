package com.ognjenlazic.LazicGym.model;

public class ClanskaKarta {
	
	public ClanskaKarta(
			int id,
			int popust,
			int brojPoena,
			Korisnik korisnik,
			boolean iskoriscena,
			boolean prihvacen,
			boolean aktivan
			) {
		this.id = id;
		this.popust = popust;
		this.brojPoena = brojPoena;
		this.korisnik = korisnik;
		this.iskoriscena = iskoriscena;
		this.prihvacen = prihvacen;
		this.aktivan = aktivan;
	}
	
	public ClanskaKarta() {
		
	}
	
	private int id; 
	private int popust;
	private int brojPoena;
	private Korisnik korisnik;
	private boolean iskoriscena; 
	private boolean prihvacen;
	private boolean aktivan;
	
	public int getBrojPoena() {
		return brojPoena;
	}
	public void setBrojPoena(int brojPoena) {
		this.brojPoena = brojPoena;
	}
	public int getPopust() {
		return popust;
	}
	public void setPopust(int popust) {
		this.popust = popust;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public boolean isPrihvacen() {
		return prihvacen;
	}
	public void setPrihvacen(boolean prihvacen) {
		this.prihvacen = prihvacen;
	}
	public boolean isAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isIskoriscena() {
		return iskoriscena;
	}
	public void setIskoriscena(boolean iskoriscena) {
		this.iskoriscena = iskoriscena;
	} 

}
