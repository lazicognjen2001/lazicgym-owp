package com.ognjenlazic.LazicGym.model;

public class Izvjestaj {
	public Izvjestaj (
			int treningId,
			String naziv,
			String trener,
			int brojTreninga,
			int ukupnaCijena
			) {
		this.treningID = treningId;
		this.naziv = naziv;
		this.trener = trener;
		this.brojTreninga = brojTreninga;
		this.ukupnaCijena = ukupnaCijena;
	}
	
	public int getTreningID() {
		return treningID;
	}
	public void setTreningID(int treningID) {
		this.treningID = treningID;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getTrener() {
		return trener;
	}

	public void setTrener(String trener) {
		this.trener = trener;
	}

	public int getBrojTreninga() {
		return brojTreninga;
	}

	public void setBrojTreninga(int brojTreninga) {
		this.brojTreninga = brojTreninga;
	}

	public int getUkupnaCijena() {
		return ukupnaCijena;
	}

	public void setUkupnaCijena(int ukupnaCijena) {
		this.ukupnaCijena = ukupnaCijena;
	}

	private int treningID;
	private String naziv;
	private String trener;
	private int brojTreninga;
	private int ukupnaCijena;
}
