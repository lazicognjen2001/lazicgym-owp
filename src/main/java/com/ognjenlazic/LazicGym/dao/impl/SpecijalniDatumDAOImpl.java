package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.SpecijalniDatumDAO;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.SpecijalniDatum;

@Repository
public class SpecijalniDatumDAOImpl implements SpecijalniDatumDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
private class SpecijalniDatumRowCallbackHandler implements RowCallbackHandler {
	
		private Map<Integer, SpecijalniDatum> datumi = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			LocalDateTime datum = rs.getTimestamp(index++).toLocalDateTime();
			int popust = rs.getInt(index++);
			boolean aktivan = rs.getBoolean(index++);
			
			SpecijalniDatum specijalniDatum = new SpecijalniDatum(id, datum, popust, aktivan);
			datumi.put(id, specijalniDatum);
		}
		
		public List<SpecijalniDatum> getSpecijalneDatume(){
			return new ArrayList<>(datumi.values());
		}

}

@Override
public SpecijalniDatum findOne(int id) {
	String sql = "SELECT id, datum, popust, aktivan FROM gym.specijalnidatum WHERE id = ?";
	SpecijalniDatumRowCallbackHandler rowCallbackHandler = new SpecijalniDatumRowCallbackHandler();
	jdbcTemplate.query(sql, rowCallbackHandler, id);
	return rowCallbackHandler.getSpecijalneDatume().get(0);
}

@Override
public List<SpecijalniDatum> findAll() {
	String sql = "SELECT id, datum, popust, aktivan FROM gym.specijalnidatum";
	SpecijalniDatumRowCallbackHandler rowCallbackHandler = new SpecijalniDatumRowCallbackHandler();
	jdbcTemplate.query(sql, rowCallbackHandler);
	return rowCallbackHandler.getSpecijalneDatume();
}

@Override
public int save(SpecijalniDatum specijalniDatum) {
	PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

		@Override
		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			String sql = "INSERT INTO specijalnidatum VALUES( ?, ?, ?, ?)";
			
			PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
			int index = 1; 
			preparedStatement.setInt(index++, specijalniDatum.getId());
			preparedStatement.setTimestamp(index++, Timestamp.valueOf(specijalniDatum.getDatum()));
			preparedStatement.setInt(index++, specijalniDatum.getPopust());
			preparedStatement.setBoolean(index++, specijalniDatum.isAktivan());
			
			return preparedStatement;
		}
		
	};
	
	boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
	return uspeh ? 1 : 0;
}

@Override
public int update(SpecijalniDatum specijalniDatum) {
	String sql = "UPDATE specijalnidatum SET datum = ?, popust = ?, aktivan = ? WHERE id = ?";
	boolean uspeh = jdbcTemplate.update(sql, 
			specijalniDatum.getDatum(), 
			specijalniDatum.getPopust(), 
			specijalniDatum.isAktivan(), 
			specijalniDatum.getId()) == 1;
	
	return uspeh ? 1 : 0;
}

@Override
public int delete(SpecijalniDatum specijalniDatum) {
	// TODO Auto-generated method stub
	return 0;
}	
}
