package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.KomentarDAO;
import com.ognjenlazic.LazicGym.dao.KorisnikDAO;
import com.ognjenlazic.LazicGym.dao.TreningDAO;
import com.ognjenlazic.LazicGym.model.Komentar;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.enumeration.StatusKomentara;
import com.ognjenlazic.LazicGym.model.enumeration.Uloga;

@Repository
public class KomentarDAOImpl implements KomentarDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private TreningDAO treningDao;
	@Autowired
	private KorisnikDAO korisnikDao;
	
private class KomentarRowCallbackHandler implements RowCallbackHandler {
		
		private Map<Integer, Komentar> komentari = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			String tekstKomentara = rs.getString(index++);
			int ocjena = rs.getInt(index++);
			LocalDateTime datumPostavljanja = rs.getTimestamp(index++).toLocalDateTime();
			int clanId = rs.getInt(index++);
			int treningId = rs.getInt(index++);
			StatusKomentara statusKomentara = StatusKomentara.valueOf(rs.getString(index++));
			boolean anoniman = rs.getBoolean(index++);
			boolean aktivan = rs.getBoolean(index++);
			Korisnik korisnik;
			Trening trening  = treningDao.findOne(treningId);
			
			
			
			System.out.println(clanId == 0);
			
			
			if(clanId == 0) {
				korisnik = null;
			} else {
				korisnik = korisnikDao.findOne(clanId);
			}

			
			Komentar komentar = new Komentar(id, tekstKomentara ,ocjena, datumPostavljanja, korisnik, trening, statusKomentara, anoniman, aktivan);
			komentari.put(id, komentar);
		}
		
		public List<Komentar> getKomentare(){
			return new ArrayList<>(komentari.values());
		}
		
	}

	@Override
	public Komentar findOne(int id) {
		String sql = "SELECT id, tekstKomentara, ocjena, datumPostavljanja, clanID, treningID, statusKomentara, anoniman, aktivan FROM gym.komentar WHERE id = ?";
		KomentarRowCallbackHandler rowCallbackHandler = new KomentarRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getKomentare().get(0);
	}

	@Override
	public List<Komentar> findAll() {
		String sql = "SELECT id, tekstKomentara, ocjena, datumPostavljanja, clanID, treningID, statusKomentara, anoniman, aktivan FROM gym.komentar";
		KomentarRowCallbackHandler rowCallbackHandler = new KomentarRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getKomentare();
	}

	@Override
	public int save(Komentar komentar) {
		PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO gym.komentar VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				
				if(komentar.getKorisnik() == null ) {
					PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
					int index = 1; 
					preparedStatement.setInt(index++, komentar.getId());
					preparedStatement.setString(index++, komentar.getTekst());
					preparedStatement.setInt(index++, komentar.getTrening().getId());
					preparedStatement.setTimestamp(index++, Timestamp.valueOf(komentar.getDatumPostavljanja()));
					preparedStatement.setString(index++, null);
					preparedStatement.setInt(index++, komentar.getTrening().getId());
					preparedStatement.setString(index++, komentar.getStatus().toString());
					preparedStatement.setBoolean(index++, komentar.isAnoniman());
					preparedStatement.setBoolean(index++, komentar.isAktivan());
					System.out.println(preparedStatement);
					return preparedStatement;
				} else {
					PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
					int index = 1; 
					preparedStatement.setInt(index++, komentar.getId());
					preparedStatement.setString(index++, komentar.getTekst());
					preparedStatement.setInt(index++, komentar.getTrening().getId());
					preparedStatement.setTimestamp(index++, Timestamp.valueOf(komentar.getDatumPostavljanja()));
					preparedStatement.setInt(index++, komentar.getKorisnik().getId());
					preparedStatement.setInt(index++, komentar.getTrening().getId());
					preparedStatement.setString(index++, komentar.getStatus().toString());
					preparedStatement.setBoolean(index++, komentar.isAnoniman());
					preparedStatement.setBoolean(index++, komentar.isAktivan());
					System.out.println(preparedStatement);
					return preparedStatement;
					
				}
				
				}
			
		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public int update(Komentar komentar) {
		
		String sql = "UPDATE komentar SET tekstKomentara = ?, ocjena = ?, datumPostavljanja = ?, clanID = ?, treningID = ?, statusKomentara = ?, anoniman = ?, aktivan = ? WHERE id = ?";
		
		if(komentar.getKorisnik() != null) {
			boolean uspeh = jdbcTemplate.update(sql, 
					komentar.getTekst(),
					komentar.getOcjena(),
					komentar.getDatumPostavljanja(), 
					komentar.getKorisnik().getId(), 
					komentar.getTrening().getId(),
					komentar.getStatus().toString(),
					komentar.isAnoniman(), 
					komentar.isAktivan(),
					komentar.getId()) == 1;
			
			return uspeh ? 1 : 0;
		} else {
			
			boolean uspeh = jdbcTemplate.update(sql, 
					komentar.getTekst(),
					komentar.getOcjena(),
					komentar.getDatumPostavljanja(), 
					null, 
					komentar.getTrening().getId(),
					komentar.getStatus().toString(),
					komentar.isAnoniman(), 
					komentar.isAktivan(),
					komentar.getId()) == 1;
			
			return uspeh ? 1 : 0;
		}
		
	}

	@Override
	public int delete(Komentar komentar) {
		String sql = "DELETE from komentar WHERE id = ?";
		
		boolean uspeh = jdbcTemplate.update(sql, komentar.getId()) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public Komentar findOne(String email, String sifra) {
		// TODO Auto-generated method stub
		return null;
	}

}
