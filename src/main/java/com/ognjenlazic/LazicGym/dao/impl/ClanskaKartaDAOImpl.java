package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.ClanskaKartaDAO;
import com.ognjenlazic.LazicGym.dao.KorisnikDAO;
import com.ognjenlazic.LazicGym.model.ClanskaKarta;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.Trening;
@Repository
public class ClanskaKartaDAOImpl implements ClanskaKartaDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private KorisnikDAO korisnikDao; 

	private class ClanskaKartaRowCallbackHandler implements RowCallbackHandler {
		
		private Map<Integer, ClanskaKarta> clanskeKarte = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			int popus = rs.getInt(index++);
			int brojPoena = rs.getInt(index++);
			int clanId = rs.getInt(index++);
			boolean prihvacen = rs.getBoolean(index++);
			boolean iskoriscena = rs.getBoolean(index++);
			boolean aktivan = rs.getBoolean(index++);
			
			System.out.println(clanId);
			Korisnik korisnik = korisnikDao.findOne(clanId);
			
			ClanskaKarta clanskaKarta = new ClanskaKarta(id, popus, brojPoena, korisnik, iskoriscena ,prihvacen , aktivan);
			clanskeKarte.put(id, clanskaKarta);
		}
		
		public List<ClanskaKarta> getCalnskeKarte(){
			return new ArrayList<>(clanskeKarte.values());
		}
		
	}
	
	@Override
	public ClanskaKarta findOne(int id) {
		String sql = "SELECT id, popust, brojPoena, clanID, prihvacen, iskoricena, aktivan FROM gym.clanskakarta WHERE id = ?";
		ClanskaKartaRowCallbackHandler rowCallbackHandler = new ClanskaKartaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getCalnskeKarte().get(0);
	}

	@Override
	public List<ClanskaKarta> findAll() {
		String sql = "SELECT id, popust, brojPoena, clanID, prihvacen, iskoricena, aktivan FROM gym.clanskakarta";
		ClanskaKartaRowCallbackHandler rowCallbackHandler = new ClanskaKartaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getCalnskeKarte();
	}

	@Override
	public int save(ClanskaKarta clanskaKarta) {
		PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO clanskakarta VALUES( ?, ?, ?, ?, ?, ?, ?)";
				
				PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
				int index = 1; 
				preparedStatement.setInt(index++, clanskaKarta.getId());
				preparedStatement.setInt(index++, clanskaKarta.getPopust());
				preparedStatement.setInt(index++, clanskaKarta.getBrojPoena());
				preparedStatement.setInt(index++, clanskaKarta.getKorisnik().getId());
				preparedStatement.setBoolean(index++, clanskaKarta.isPrihvacen());
				preparedStatement.setBoolean(index++, clanskaKarta.isIskoriscena());
				preparedStatement.setBoolean(index++, clanskaKarta.isAktivan());
				
				return preparedStatement;
			}
			
		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public int update(ClanskaKarta clanskaKarta) {
		String sql = "UPDATE clanskakarta SET popust = ?, brojPoena = ?, clanID = ?, prihvacen = ?, iskoricena = ?, aktivan = ? WHERE id = ?";
		boolean uspeh = jdbcTemplate.update(sql, 
				clanskaKarta.getPopust(), 
				clanskaKarta.getBrojPoena(),
				clanskaKarta.getKorisnik().getId(),
				clanskaKarta.isPrihvacen(),
				clanskaKarta.isIskoriscena(), 
				clanskaKarta.isAktivan(), 
				
				clanskaKarta.getId()) == 1;
		
		return uspeh ? 1 : 0;
	}

	@Override
	public int delete(ClanskaKarta clanskaKarta) {
		String sql = "DELETE from clanskakarta WHERE id = ?";
		
		boolean uspeh = jdbcTemplate.update(sql, clanskaKarta.getId()) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public List<ClanskaKarta> find(String naziv, String trener, Integer cijenaOd, Integer cijenaDo,
			String vrstaTreninga, String nivoTreninga, Integer trejenjeOd, Integer trajanjeDo,
			String sortiranjeOcjena) {
		// TODO Auto-generated method stub
		return null;
	}

}
