package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.SalaDAO;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.TipTreninga;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.enumeration.NivoTreninga;
import com.ognjenlazic.LazicGym.model.enumeration.VrstaTreninga;


@Repository
public class SalaDAOImpl implements SalaDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
private class SalaRowCallbackHandler implements RowCallbackHandler {
	
		private Map<Integer, Sala> sale = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			String oznakaSale = rs.getString(index++);
			int kapacitet = rs.getInt(index++);
			boolean popunjen = rs.getBoolean(index++);
			boolean aktivan = rs.getBoolean(index++);
			
			Sala termin = new Sala(oznakaSale, kapacitet, id, popunjen, aktivan);
			sale.put(id, termin);
		}
		
		public List<Sala> getSale(){
			return new ArrayList<>(sale.values());
		}

}	

	@Override
	public Sala findOne(int id) {
		String sql = "SELECT id, oznakaSale, kapacitet, popunjen, aktivan FROM gym.sala WHERE id = ?";
		SalaRowCallbackHandler rowCallbackHandler = new SalaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getSale().get(0);
	}

	@Override
	public List<Sala> findAll() {
		String sql = "SELECT id, oznakaSale, kapacitet, popunjen, aktivan FROM gym.sala";
		SalaRowCallbackHandler rowCallbackHandler = new SalaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getSale();
	}

	@Override
	public int save(Sala sala) {
		PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO sala VALUES( ?, ?, ?, ?, ?)";
				
				PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
				int index = 1; 
				preparedStatement.setInt(index++, sala.getId());
				preparedStatement.setString(index++, sala.getOznaka());
				preparedStatement.setInt(index++, sala.getKapacitet());
				preparedStatement.setBoolean(index++, sala.getPopunjen());
				preparedStatement.setBoolean(index++, sala.getAktivan());
				
				return preparedStatement;
			}
			
		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public int update(Sala sala) {
		String sql = "UPDATE sala SET oznakaSale = ?, kapacitet = ?, popunjen = ?, aktivan = ? WHERE id = ?";
		boolean uspeh = jdbcTemplate.update(sql, 
				sala.getOznaka(), 
				sala.getKapacitet(), 
				sala.getPopunjen(), 
				sala.getAktivan(), 
				sala.getId()) == 1;
		
		return uspeh ? 1 : 0;
	}

	@Override
	public int delete(Sala sala) {
		String sql = "DELETE from sala WHERE id = ?";
		
		boolean uspeh = jdbcTemplate.update(sql, sala.getId()) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public List<Sala> findAll(String oznaka, Integer kapacitet) {
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT s.id, s.oznakaSale, s.kapacitet, s.popunjen, s.aktivan FROM gym.sala s ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE "); 
		boolean imaArgumenata = false;
		
		if(oznaka!=null) { 
			oznaka = "%" + oznaka + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("s.oznakaSale LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(oznaka); 
			}
		
		if(kapacitet !=null) { 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("s.kapacitet LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(kapacitet); 
			}
		if(imaArgumenata) {
			sql=sql + whereSql.toString();

		}
			List<Sala> sale = jdbcTemplate.query(sql,  listaArgumenata.toArray(), new SalaRowMapper());
		
			return sale;
	}
	
	private class SalaRowMapper implements RowMapper<Sala> {

		@Override
		public Sala mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			String oznaka = rs.getString(index++);
			int kapacitet = rs.getInt(index++); 
			boolean popunjen = rs.getBoolean(index++);
			boolean aktivan = rs.getBoolean(index++);

			Sala sala = new Sala(oznaka, kapacitet, id, popunjen, aktivan);
			return sala;
		}

	}
	

}
