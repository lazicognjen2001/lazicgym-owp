package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.TreningDAO;
import com.ognjenlazic.LazicGym.model.TipTreninga;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.enumeration.NivoTreninga;
import com.ognjenlazic.LazicGym.model.enumeration.VrstaTreninga;
import com.ognjenlazic.LazicGym.service.TipTreningaService;

@Repository
public class TreningDAOImpl implements TreningDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private TipTreningaService tipTreningaService;
	
	private class TreningRowCallbackHandler implements RowCallbackHandler {
		
		private Map<Integer, Trening> treninzi = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			String naziv = rs.getString(index++);
			String trener = rs.getString(index++);
			String opis = rs.getString(index++);
			int tipTreningaId = rs.getInt(index++); // Pronaći tip treninga uz poomoć find one metode - trenutno se ne koristi jer nije bilo DAO
			int cijena = rs.getInt(index++);
			VrstaTreninga vrstaTreninga = VrstaTreninga.valueOf(rs.getString(index++));
			NivoTreninga nivoTreninga =NivoTreninga.valueOf(rs.getString(index++));
			String urlSlike = rs.getString(index++);
			int trajanjeTreninga = rs.getInt(index++);
			double prosjecnaOcjena = rs.getDouble(index++);
			boolean aktivan = rs.getBoolean(index++);
			TipTreninga tip = tipTreningaService.findOne(tipTreningaId);
			Trening trening = new Trening(id, naziv, trener, opis, cijena, vrstaTreninga, nivoTreninga, tip, urlSlike, trajanjeTreninga, prosjecnaOcjena, aktivan);
			
			treninzi.put(id, trening);
		}
		
		public List<Trening> getTreninge(){
			return new ArrayList<>(treninzi.values());
		}
		
	}

	@Override
	public Trening findOne(int id) {
		String sql = "SELECT id, naziv, trener, opis, tipTreningaID, cijena, vrstaTreninga, nivoTreninga, slika, trajanjeTreninga, prosjecnaOcena, aktivan FROM gym.trening WHERE id = ?";
		TreningRowCallbackHandler rowCallbackHandler = new TreningRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getTreninge().get(0);
	}

	@Override
	public List<Trening> findAll() {
		String sql = "SELECT id, naziv, trener, opis, tipTreningaID, cijena, vrstaTreninga, nivoTreninga, slika, trajanjeTreninga, prosjecnaOcena, aktivan FROM gym.trening";
		TreningRowCallbackHandler rowCallbackHandler = new TreningRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getTreninge();
	}

	@Override
	public int save(Trening trening) {
		PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO trening VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				
				PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
				int index = 1; 
				preparedStatement.setInt(index++, trening.getId());
				preparedStatement.setString(index++, trening.getNaziv());
				preparedStatement.setString(index++, trening.getTrener());
				preparedStatement.setString(index++, trening.getOpis());
				preparedStatement.setInt(index++, trening.getTip().getId()); // Izmjeniti ovaj dio da prima tip treninga
				preparedStatement.setInt(index++, trening.getCijena());
				preparedStatement.setString(index++, trening.getVrsta().toString());
				preparedStatement.setString(index++, trening.getNivoTreninga().toString());
				preparedStatement.setString(index++, trening.getSlika());
				preparedStatement.setInt(index++, trening.getTrajanjeTreninga());
				preparedStatement.setDouble(index++, trening.getProsjecnaOcjena());
				preparedStatement.setBoolean(index++, trening.isAktivan());
				
				return preparedStatement;
			}
			
		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public int update(Trening trening) {
		
		String sql = "UPDATE trening SET id = ?, naziv = ?, trener = ?, opis = ?, cijena = ?, vrstaTreninga = ?, nivoTreninga = ?, slika = ?, trajanjeTreninga = ?, prosjecnaOcena = ?, aktivan = ? WHERE id = ?";
		boolean uspeh = jdbcTemplate.update(sql, trening.getId(), 
				trening.getNaziv(), 
				trening.getTrener(), 
				trening.getOpis(), 
				trening.getCijena(), 
				trening.getVrsta().toString(), 
				trening.getNivoTreninga().toString(), 
				trening.getSlika(), 
				trening.getTrajanjeTreninga(), 
				trening.getProsjecnaOcjena(), 
				trening.isAktivan(),
				trening.getId()) == 1;
		
		return uspeh ? 1 : 0;
	}

	@Override
	public int delete(Trening trening) {
		String sql = "DELETE from trening WHERE id = ?";
		boolean uspeh = jdbcTemplate.update(sql, trening.getId()) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public List<Trening> find(String naziv, String trener, Integer cijenaOd, Integer cijenaDo, String vrstaTreninga,
			String nivoTreninga, Integer trajanjeOd, Integer trajanjeDo, String sortiranjeOcjena) {

		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		String sql = "SELECT t.id, t.naziv, t.trener, t.opis, t.tipTreningaID, t.cijena, t.vrstaTreninga, t.nivoTreninga, t.slika, t.trajanjeTreninga, t.prosjecnaOcena, t.aktivan FROM trening t ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE "); 
		boolean imaArgumenata = false;
		
		if(naziv!=null) { 
			naziv = "%" + naziv + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("t.naziv LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(naziv); 
			}
		
		if(trener !=null) { 
			trener = "%" + trener + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("t.trener LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(trener); 
			}
		
		if(vrstaTreninga !=null) { 
			vrstaTreninga = "%" + vrstaTreninga + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("t.vrstaTreninga LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(vrstaTreninga); 
			}
		
		if(nivoTreninga !=null) { 
			nivoTreninga = "%" + nivoTreninga + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("t.nivoTreninga LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(nivoTreninga);
		}
			
			if(trajanjeOd != null) { 
				if(imaArgumenata) 
					whereSql.append(" AND ");
			 whereSql.append("t.trajanjeTreninga >= ?"); 
			 imaArgumenata = true;
			 listaArgumenata.add(trajanjeOd); 
			 }
			
			if(trajanjeDo!=null) { 
			if(imaArgumenata) 
			 whereSql.append(" AND ");
			 whereSql.append("t.trajanjeTreninga <= ?"); 
			 imaArgumenata = true;
			 listaArgumenata.add(trajanjeDo); 
			 }
			
			if(cijenaOd != null) { 
				if(imaArgumenata) 
					whereSql.append(" AND ");
			 whereSql.append("t.cijena >= ?"); 
			 imaArgumenata = true;
			 listaArgumenata.add(cijenaOd); 
			 }
			
			if(cijenaDo!=null) { 
			if(imaArgumenata) 
			 whereSql.append(" AND ");
			 whereSql.append("t.cijena <= ?"); 
			 imaArgumenata = true;
			 listaArgumenata.add(cijenaDo); 
			 }
			
			if (sortiranjeOcjena == null) {
				if(imaArgumenata) 
					sql=sql + whereSql.toString()+" ORDER BY t.id ";// + sortiranjeOcjena; 
				else
					sql=sql + " ORDER BY t.id ";// + sortiranjeOcjena; 
					System.out.println(sql);
			} else {
				if(imaArgumenata) 
					sql=sql + whereSql.toString()+" ORDER BY t.prosjecnaOcena " + sortiranjeOcjena; 
				else
					sql=sql + " ORDER BY t.prosjecnaOcena " + sortiranjeOcjena; 
					System.out.println(sql);
			}
			
			
				List<Trening> treninzi = jdbcTemplate.query(sql,  listaArgumenata.toArray(), new FilmRowMapper());
		
		return treninzi;
	}	
	
	private class FilmRowMapper implements RowMapper<Trening> {

		@Override
		public Trening mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			String naziv = rs.getString(index++);
			String trener = rs.getString(index++);
			String opis = rs.getString(index++);
			int tipTreningaId = rs.getInt(index++); // Pronaći tip treninga uz poomoć find one metode
			int cijena = rs.getInt(index++);
			VrstaTreninga vrstaTreninga = VrstaTreninga.valueOf(rs.getString(index++));
			NivoTreninga nivoTreninga =NivoTreninga.valueOf(rs.getString(index++));
			String urlSlike = rs.getString(index++);
			int trajanjeTreninga = rs.getInt(index++);
			double prosjecnaOcjena = rs.getDouble(index++);
			boolean aktivan = rs.getBoolean(index++);
			TipTreninga tip = tipTreningaService.findOne(tipTreningaId);
			Trening trening = new Trening(id, naziv, trener, opis, cijena, vrstaTreninga, nivoTreninga, tip, urlSlike, trajanjeTreninga, prosjecnaOcjena, aktivan);
			return trening;
		}

	}
	

}
