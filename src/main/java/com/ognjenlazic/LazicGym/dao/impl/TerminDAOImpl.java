package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.KorisnikDAO;
import com.ognjenlazic.LazicGym.dao.SalaDAO;
import com.ognjenlazic.LazicGym.dao.TerminDAO;
import com.ognjenlazic.LazicGym.dao.TreningDAO;
import com.ognjenlazic.LazicGym.model.Izvjestaj;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.TipTreninga;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.enumeration.NivoTreninga;
import com.ognjenlazic.LazicGym.model.enumeration.VrstaTreninga;

@Repository
public class TerminDAOImpl implements TerminDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private SalaDAO salaDao;
	@Autowired
	private TreningDAO treningDao;
	@Autowired
	private KorisnikDAO korisnikDao; 
	
	private class TerminRowCallbackHandler implements RowCallbackHandler {
		
		private Map<Integer, Termin> termini = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			int salaId = rs.getInt(index++);
			int treningId = rs.getInt(index++);
			int clanId = rs.getInt(index++);
			LocalDateTime datumOdrzavanja = rs.getTimestamp(index++).toLocalDateTime();
			boolean aktivan = rs.getBoolean(index++);
			boolean popunjen = rs.getBoolean(index++);
			
			Trening trening  = treningDao.findOne(treningId);
			Sala sala = salaDao.findOne(salaId);
			
			Korisnik korisnik;
			if(clanId != 0) {
				korisnik = korisnikDao.findOne(clanId);
			} else {
				korisnik = null;
			}
			
			Termin termin = new Termin(id, sala, trening, korisnik, datumOdrzavanja, popunjen, aktivan);
			termini.put(id, termin);
		}
		
		public List<Termin> getTermini(){
			return new ArrayList<>(termini.values());
		}
		
	}

	@Override
	public Termin findOne(int id) {
		String sql = "SELECT id, salaID, treningID, clanID, datumOdrzavanja, aktivan, popunjen FROM gym.termintreninga WHERE id = ?";
		TerminRowCallbackHandler rowCallbackHandler = new TerminRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getTermini().get(0);
	}

	@Override
	public List<Termin> findAll() {
		String sql = "SELECT id, salaID, treningID, clanID, datumOdrzavanja, aktivan, popunjen FROM gym.termintreninga";
		TerminRowCallbackHandler rowCallbackHandler = new TerminRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getTermini();
	}

	@Override
	public int save(Termin termin) {
		PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO termintreninga VALUES( ?, ?, ?, ?, ?, ?, ?)";
				if(termin.getKorisnik() != null) {
				PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
				int index = 1; 
				preparedStatement.setInt(index++, termin.getId());
				preparedStatement.setInt(index++, termin.getSala().getId());
				preparedStatement.setInt(index++, termin.getTrening().getId());
				preparedStatement.setInt(index++, termin.getKorisnik().getId());
				preparedStatement.setTimestamp(index++, Timestamp.valueOf(termin.getDatumIVrijemeTreninga()));
				preparedStatement.setBoolean(index++, termin.isAktivan());
				preparedStatement.setBoolean(index++, termin.isPopunjen());
				return preparedStatement;
				} else {
					PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
					int index = 1; 
					preparedStatement.setInt(index++, termin.getId());
					preparedStatement.setInt(index++, termin.getSala().getId());
					preparedStatement.setInt(index++, termin.getTrening().getId());
					preparedStatement.setString(index++, null);
					preparedStatement.setTimestamp(index++, Timestamp.valueOf(termin.getDatumIVrijemeTreninga()));
					preparedStatement.setBoolean(index++, termin.isAktivan());
					preparedStatement.setBoolean(index++, termin.isPopunjen());
					return preparedStatement;
				}
				
			}
			
		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public int update(Termin termin) {
		String sql = "UPDATE gym.termintreninga SET salaID = ?, treningID = ?, clanID = ?, datumOdrzavanja = ?, aktivan = ?, popunjen = ? WHERE id = ?";
		
		if(termin.getKorisnik() == null) {
			boolean uspeh = jdbcTemplate.update(sql, 
					termin.getSala().getId(),
					termin.getTrening().getId(), 
					null, 
					termin.getDatumIVrijemeTreninga(), 
					termin.isAktivan(), 
					termin.isPopunjen(),
					termin.getId()) == 1;
			System.out.println("Izvrsavanje Cuvanja TERMIN: " + termin);
			return uspeh ? 1 : 0;
		} else {
			boolean uspeh = jdbcTemplate.update(sql, 
					termin.getSala().getId(),
					termin.getTrening().getId(), 
					termin.getKorisnik().getId(), 
					termin.getDatumIVrijemeTreninga(), 
					termin.isAktivan(), 
					termin.isPopunjen(),
					termin.getId()) == 1;
			System.out.println("Izvrsavanje Cuvanja TERMIN: " + termin);
			return uspeh ? 1 : 0;
		}
		
		
	}

	@Override
	public int delete(Termin termin) {
		String sql = "DELETE from termintreninga WHERE id = ?";
		
		boolean uspeh = jdbcTemplate.update(sql, termin.getId()) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public Termin findOne(String email, String sifra) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Izvjestaj> getIzvjestaj(LocalDateTime datumOd, LocalDateTime datumDo) {
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		String sql = "select treningID, trening.naziv, trening.trener, count(*) as Broj_treninga, count(*) * trening.cijena as Ukupna_cijena from gym.termintreninga as termini, gym.trening as trening";
		// where termini.datumOdrzavanja between ? and ? and trening.id = termini.treningID group by treningID
		StringBuffer whereSql = new StringBuffer(" WHERE "); 
		

		 whereSql.append("termini.datumOdrzavanja between ? and ? and trening.id = termini.treningID group by treningID"); 
		 listaArgumenata.add(datumOd);
		 listaArgumenata.add(datumDo);
		 sql=sql + whereSql.toString();
		List<Izvjestaj> izvjestaji = jdbcTemplate.query(sql,  listaArgumenata.toArray(), new IzvjestajRowMapper());
		return izvjestaji;
	}
	



private class IzvjestajRowMapper implements RowMapper<Izvjestaj> {

	@Override
	public Izvjestaj mapRow(ResultSet rs, int rowNum) throws SQLException {
		int index = 1;
		
		int treningId = rs.getInt(index++);
		String naziv = rs.getString(index++);
		String trener = rs.getString(index++);
		int brojtreninga = rs.getInt(index++);
		int ukupnaCijena = rs.getInt(index++);
		System.out.println("IZVJESTAJ: " + treningId + " " + naziv);
		Izvjestaj izvjestaj = new Izvjestaj(treningId, naziv, trener, brojtreninga, ukupnaCijena);
		return izvjestaj;
	}

}

@Override
public List<Termin> findAll(LocalDateTime datumOd, LocalDateTime datumDo) {

	ArrayList<Object> listaArgumenata = new ArrayList<Object>();
	String sql = "SELECT id, salaID, treningID, clanID, datumOdrzavanja, aktivan, popunjen FROM gym.termintreninga";

	StringBuffer whereSql = new StringBuffer(" WHERE "); 
	boolean imaArgumenata = false;
	
	 whereSql.append("datumOdrzavanja between ? and ? "); 
	 listaArgumenata.add(datumOd);
	 listaArgumenata.add(datumDo);
	 sql=sql + whereSql.toString();
		List<Termin> termini = jdbcTemplate.query(sql,  listaArgumenata.toArray(), new TerminRowMapper());

	
	return termini;
}

private class TerminRowMapper implements RowMapper<Termin> {

	@Override
	public Termin mapRow(ResultSet rs, int rowNum) throws SQLException {
		int index = 1;
		
		int id = rs.getInt(index++);
		int salaId = rs.getInt(index++);
		int treningId = rs.getInt(index++);
		int clanId = rs.getInt(index++);
		LocalDateTime datumOdrzavanja = rs.getTimestamp(index++).toLocalDateTime();
		boolean aktivan = rs.getBoolean(index++);
		boolean popunjen = rs.getBoolean(index++);
		
		Trening trening  = treningDao.findOne(treningId);
		Sala sala = salaDao.findOne(salaId);
		
		Korisnik korisnik;
		if(clanId != 0) {
			korisnik = korisnikDao.findOne(clanId);
		} else {
			korisnik = null;
		}
		
		Termin termin = new Termin(id, sala, trening, korisnik, datumOdrzavanja, popunjen, aktivan);
		return termin;
	}

}

}
