package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.KorisnikDAO;
import com.ognjenlazic.LazicGym.dao.ListaZeljaDAO;
import com.ognjenlazic.LazicGym.dao.TreningDAO;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.Zelja;

@Repository
public class ListaZeljaDAOImpl implements ListaZeljaDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private TreningDAO treningDao;
	@Autowired
	private KorisnikDAO korisnikDao; 
	
	
	private class ZeljaRowCallbackHandler implements RowCallbackHandler {
		
		private Map<Integer, Zelja> zelje = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			int korisnikId = rs.getInt(index++);
			int treningId = rs.getInt(index++);
			boolean aktivan = rs.getBoolean(index++);
			
			Korisnik korisnik = korisnikDao.findOne(korisnikId);
			Trening trening = treningDao.findOne(treningId);
			
			Zelja zelja = new Zelja(id, korisnik, trening, aktivan);
			zelje.put(id, zelja);
			
		}
		
		public List<Zelja> getZelje(){
			return new ArrayList<>(zelje.values());
		}
		
	}

	@Override
	public Zelja findOne(int id) {
		String sql = "SELECT id, clanID, treningID, aktivan FROM gym.listazelja WHERE id = ?";
		ZeljaRowCallbackHandler rowCallbackHandler = new ZeljaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getZelje().get(0);
	}

	@Override
	public List<Zelja> findAll() {
		String sql = "SELECT id, clanID, treningID, aktivan FROM gym.listazelja";
		ZeljaRowCallbackHandler rowCallbackHandler = new ZeljaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getZelje();
	}

	@Override
	public int save(Zelja zelja) {
		PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO listazelja VALUES( ?, ?, ?, ?)";
				
				PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
				int index = 1; 
				preparedStatement.setInt(index++, zelja.getId());
				preparedStatement.setInt(index++, zelja.getKorisnik().getId());
				preparedStatement.setInt(index++, zelja.getTrening().getId());
				preparedStatement.setBoolean(index++, zelja.isAktivan());
			
				
				return preparedStatement;
			}
			
		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public int update(Zelja zelja) {
		String sql = "UPDATE listazelja SET clanID = ?, treningID = ?, aktivan = ? WHERE id = ?";
		boolean uspeh = jdbcTemplate.update(sql,
				zelja.getKorisnik().getId(), 
				zelja.getTrening().getId(), 
				zelja.isAktivan(), 
				zelja.getId()) == 1;
		
		return uspeh ? 1 : 0;
	}

	@Override
	public int delete(Zelja zelja) {
		String sql = "DELETE from korisnici WHERE id = ?";
		
		boolean uspeh = jdbcTemplate.update(sql, zelja.getId()) == 1;
		return uspeh ? 1 : 0;
	}

}
