package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.KorisnikDAO;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.TipTreninga;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.enumeration.NivoTreninga;
import com.ognjenlazic.LazicGym.model.enumeration.Uloga;
import com.ognjenlazic.LazicGym.model.enumeration.VrstaTreninga;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class KorisnikRowCallbackHandler implements RowCallbackHandler {
		
		private Map<Integer, Korisnik> korisnici = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			String korisnickoIme = rs.getString(index++);
			String lozinka = rs.getString(index++);
			String email = rs.getString(index++);
			String ime = rs.getString(index++);
			String prezime = rs.getString(index++);
			Date datumRodjenja = rs.getDate(index++);
			String adresa = rs.getString(index++);
			long brojTelefona = rs.getLong(index++);
			LocalDateTime datumRegistracije = rs.getTimestamp(index++).toLocalDateTime();
			Uloga tipKorisnika = Uloga.valueOf(rs.getString(index++));
			boolean blokiran = rs.getBoolean(index++);
			boolean aktivan = rs.getBoolean(index++);
			
			Korisnik korisnik = new Korisnik(id, korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumRegistracije, tipKorisnika, blokiran, aktivan);
			korisnici.put(id, korisnik);
		}
		
		public List<Korisnik> getKorisnike(){
			return new ArrayList<>(korisnici.values());
		}
		
	}

	@Override
	public Korisnik findOne(int id) {
		String sql = "SELECT id, korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumRegistracije, tipKorisnika, blokiran, aktivan FROM gym.korisnici WHERE id = ?";
		KorisnikRowCallbackHandler rowCallbackHandler = new KorisnikRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getKorisnike().get(0);
	}

	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT id, korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumRegistracije, tipKorisnika, blokiran, aktivan FROM gym.korisnici";
		KorisnikRowCallbackHandler rowCallbackHandler = new KorisnikRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getKorisnike();
	}

	@Override
	public int save(Korisnik korisnik) {
		PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO korisnici VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				
				PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
				int index = 1; 
				preparedStatement.setInt(index++, korisnik.getId());
				preparedStatement.setString(index++, korisnik.getKorisnickoIme());
				preparedStatement.setString(index++, korisnik.getLozinka());
				preparedStatement.setString(index++, korisnik.getEmail());
				preparedStatement.setString(index++, korisnik.getIme());
				preparedStatement.setString(index++, korisnik.getPrezime());
				preparedStatement.setDate(index++, (java.sql.Date) korisnik.getDatumRodjenja());
				preparedStatement.setString(index++, korisnik.getAdresa());
				preparedStatement.setLong(index++, korisnik.getBrojTelefona());
				preparedStatement.setTimestamp(index++, Timestamp.valueOf(korisnik.getDatumIVrijemeRegistracije()));
				preparedStatement.setString(index++, korisnik.getUloga().toString());
				preparedStatement.setBoolean(index++, korisnik.isBlokiran());
				preparedStatement.setBoolean(index++, korisnik.isAktivan());
				
				return preparedStatement;
			}
			
		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public int update(Korisnik korisnik) {
		String sql = "UPDATE korisnici SET korisnickoIme = ?, lozinka = ?, email = ?, ime = ?, prezime = ?, datumRodjenja = ?, adresa = ?, brojTelefona = ?, datumRegistracije = ?, tipKorisnika = ?, blokiran = ?, aktivan = ? WHERE id = ?";
		boolean uspeh = jdbcTemplate.update(sql, 
				korisnik.getKorisnickoIme(), 
				korisnik.getLozinka(), 
				korisnik.getEmail(), 
				korisnik.getIme(), 
				korisnik.getPrezime(), 
				(java.sql.Date) korisnik.getDatumRodjenja(), 
				korisnik.getAdresa(), 
				korisnik.getBrojTelefona(), 
				korisnik.getDatumIVrijemeRegistracije(), 
				korisnik.getUloga().toString(), 
				korisnik.isBlokiran(), 
				korisnik.isAktivan(),
				korisnik.getId()) == 1;
		
		return uspeh ? 1 : 0;
	}

	@Override
	public int delete(Korisnik korisnik) {
		String sql = "DELETE from korisnici WHERE id = ?";
		
		boolean uspeh = jdbcTemplate.update(sql, korisnik.getId()) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public Korisnik findOne(String email, String sifra) {
		String sql = 
				"SELECT id, korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumRegistracije, tipKorisnika, blokiran, aktivan FROM korisnici kor " + 
				"WHERE kor.email = ? AND " +
				"kor.lozinka = ? " + 
				"ORDER BY kor.id";

		KorisnikRowCallbackHandler rowCallbackHandler = new KorisnikRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, email, sifra);

		if(rowCallbackHandler.getKorisnike().size() == 0) {
			return null;
		}
		
		return rowCallbackHandler.getKorisnike().get(0);
	}

	@Override
	public List<Korisnik> findAll(String email, String korisnickoIme, String ime, String prezime) {
		String sql = "SELECT id, korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumRegistracije, tipKorisnika, blokiran, aktivan FROM gym.korisnici";
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();

		StringBuffer whereSql = new StringBuffer(" WHERE "); 
		boolean imaArgumenata = false;
		
		if(email!=null) { 
			email = "%" + email + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("email LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(email); 
			}
		
		if(korisnickoIme !=null) { 
			korisnickoIme = "%" + korisnickoIme + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("korisnickoIme LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(korisnickoIme); 
			}
		
		if(ime!=null) { 
			ime = "%" + ime + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("ime LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(ime); 
			}
		
		if(prezime !=null) { 
			prezime = "%" + prezime + "%"; 
			if(imaArgumenata)
			whereSql.append(" AND "); 
			whereSql.append("prezime LIKE ?"); 
			imaArgumenata = true; 
			listaArgumenata.add(prezime); 
			}
		
		if(imaArgumenata) {
			sql=sql + whereSql.toString();

		}
		List<Korisnik> korisnici = jdbcTemplate.query(sql,  listaArgumenata.toArray(), new KorisnikRowMapper());

		return korisnici;
	}
	
	private class KorisnikRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			String korisnickoIme = rs.getString(index++);
			String lozinka = rs.getString(index++);
			String email = rs.getString(index++);
			String ime = rs.getString(index++);
			String prezime = rs.getString(index++);
			Date datumRodjenja = rs.getDate(index++);
			String adresa = rs.getString(index++);
			long brojTelefona = rs.getLong(index++);
			LocalDateTime datumRegistracije = rs.getTimestamp(index++).toLocalDateTime();
			Uloga tipKorisnika = Uloga.valueOf(rs.getString(index++));
			boolean blokiran = rs.getBoolean(index++);
			boolean aktivan = rs.getBoolean(index++);
			
			Korisnik korisnik = new Korisnik(id, korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumRegistracije, tipKorisnika, blokiran, aktivan);
			return korisnik;
		}

	}

}
