package com.ognjenlazic.LazicGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ognjenlazic.LazicGym.dao.TipTreningaDAO;
import com.ognjenlazic.LazicGym.model.TipTreninga;

@Repository
public class TipTreningaDAOImpl implements TipTreningaDAO {
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class TipTreningaRowCallbackHandler implements RowCallbackHandler {
		
		private Map<Integer, TipTreninga> tipTreningaMap = new HashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			
			int id = rs.getInt(index++);
			String ime = rs.getString(index++);
			String opis = rs.getString(index++);
			boolean aktivan = rs.getBoolean(index++);
			
			TipTreninga tipTreninga = new TipTreninga(ime, opis, id, aktivan);
			tipTreningaMap.put(id, tipTreninga);
		}
		
		public List<TipTreninga> getTipoveTreninga(){
			return new ArrayList<>(tipTreningaMap.values());
		}
		
	}

	@Override
	public TipTreninga findOne(int id) {
		String sql = "SELECT id, ime, opis, aktivan FROM gym.tiptreninga WHERE id = ?";
		TipTreningaRowCallbackHandler rowCallbackHandler = new TipTreningaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getTipoveTreninga().get(0);
	}

	@Override
	public List<TipTreninga> findAll() {
		String sql = "SELECT id, ime, opis, aktivan FROM gym.tiptreninga";
		TipTreningaRowCallbackHandler rowCallbackHandler = new TipTreningaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getTipoveTreninga();
	}

	@Override
	public int save(TipTreninga tipTreninga) {
		PreparedStatementCreator preparedStatmentCreater = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO tiptreninga VALUES( ?, ?, ?, ?)";
				
				PreparedStatement preparedStatement = con.prepareStatement(sql); // Statement.RETURN_GENERATED_KEYS
				int index = 1; 
				preparedStatement.setInt(index++, tipTreninga.getId());
				preparedStatement.setString(index++, tipTreninga.getImeTreninga());
				preparedStatement.setString(index++, tipTreninga.getOpis());
				preparedStatement.setBoolean(index++, tipTreninga.getAktivan());
				
				return preparedStatement;
			}
			
		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatmentCreater) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public int update(TipTreninga tipTreninga) {
		String sql = "UPDATE tiptreninga SET id = ? ime = ?, opis = ?, aktivan = ? WHERE id = ?";
		boolean uspeh = jdbcTemplate.update(sql, tipTreninga.getId(), 
				tipTreninga.getImeTreninga(), 
				tipTreninga.getOpis(), 
				tipTreninga.getAktivan(),
				tipTreninga.getId()) == 1;
		
		return uspeh ? 1 : 0;
	}

	@Override
	public int delete(TipTreninga tipTreninga) {
		String sql = "DELETE from tiptreninga WHERE id = ?";
		
		boolean uspeh = jdbcTemplate.update(sql, tipTreninga.getId()) == 1;
		return uspeh ? 1 : 0;
	}

	@Override
	public TipTreninga findOne(String email, String sifra) {
		// TODO Auto-generated method stub
		return null;
	}


}
