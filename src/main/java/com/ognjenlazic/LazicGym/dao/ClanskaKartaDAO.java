package com.ognjenlazic.LazicGym.dao;

import java.util.List;

import com.ognjenlazic.LazicGym.model.ClanskaKarta;
import com.ognjenlazic.LazicGym.model.Trening;

public interface ClanskaKartaDAO {
	public ClanskaKarta findOne(int id);
	public List<ClanskaKarta> findAll();
	public int save(ClanskaKarta clanskaKarta);
	public int update(ClanskaKarta clanskaKarta);
	public int delete(ClanskaKarta clanskaKarta);
	public List<ClanskaKarta> find(String naziv, String trener, Integer cijenaOd, Integer cijenaDo, String vrstaTreninga, String nivoTreninga, Integer trejenjeOd, Integer trajanjeDo, String sortiranjeOcjena);
}
