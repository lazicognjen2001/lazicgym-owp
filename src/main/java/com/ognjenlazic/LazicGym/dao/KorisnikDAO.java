package com.ognjenlazic.LazicGym.dao;

import java.util.List;

import com.ognjenlazic.LazicGym.model.Korisnik;

public interface KorisnikDAO {
	public Korisnik findOne(int id);
	public List<Korisnik> findAll();
	public int save(Korisnik korisnik);
	public int update(Korisnik korisnik);
	public int delete(Korisnik korisnik);
	public Korisnik findOne(String email, String sifra);
	public List<Korisnik> findAll(String email, String korisnickoIme, String ime, String prezime);

}
