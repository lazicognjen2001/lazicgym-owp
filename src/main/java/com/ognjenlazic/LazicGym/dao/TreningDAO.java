package com.ognjenlazic.LazicGym.dao;

import java.util.List;

import com.ognjenlazic.LazicGym.model.Trening;

public interface TreningDAO {
	public Trening findOne(int id);
	public List<Trening> findAll();
	public int save(Trening trening);
	public int update(Trening trening);
	public int delete(Trening trening);
	public List<Trening> find(String naziv, String trener, Integer cijenaOd, Integer cijenaDo, String vrstaTreninga, String nivoTreninga, Integer trejenjeOd, Integer trajanjeDo, String sortiranjeOcjena);
}
