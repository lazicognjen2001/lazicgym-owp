package com.ognjenlazic.LazicGym.dao;

import java.util.List;

import com.ognjenlazic.LazicGym.model.TipTreninga;

public interface TipTreningaDAO {
	public TipTreninga findOne(int id);
	public List<TipTreninga> findAll();
	public int save(TipTreninga tipTreninga);
	public int update(TipTreninga tipTreninga);
	public int delete(TipTreninga tipTreninga);
	public TipTreninga findOne(String email, String sifra);
}
