package com.ognjenlazic.LazicGym.dao;

import java.util.List;

import com.ognjenlazic.LazicGym.model.SpecijalniDatum;


public interface SpecijalniDatumDAO {
	public SpecijalniDatum findOne(int id);
	public List<SpecijalniDatum> findAll();
	public int save(SpecijalniDatum specijalniDatum);
	public int update(SpecijalniDatum specijalniDatum);
	public int delete(SpecijalniDatum specijalniDatum);
	
}
