package com.ognjenlazic.LazicGym.dao;

import java.time.LocalDateTime;
import java.util.List;

import com.ognjenlazic.LazicGym.model.Izvjestaj;
import com.ognjenlazic.LazicGym.model.Termin;

public interface TerminDAO {
	public Termin findOne(int id);
	public List<Termin> findAll();
	public int save(Termin termin);
	public int update(Termin termin);
	public int delete(Termin termin);
	public Termin findOne(String email, String sifra);
	public List<Izvjestaj> getIzvjestaj(LocalDateTime datumOd, LocalDateTime datumDo);
	public List<Termin> findAll(LocalDateTime datumOd, LocalDateTime datumDo);

}
