package com.ognjenlazic.LazicGym.dao;

import java.util.List;

import com.ognjenlazic.LazicGym.model.Komentar;

public interface KomentarDAO {
	public Komentar findOne(int id);
	public List<Komentar> findAll();
	public int save(Komentar komentar);
	public int update(Komentar komentar);
	public int delete(Komentar komentar);
	public Komentar findOne(String email, String sifra);

}
