package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.ClanskaKarta;
import com.ognjenlazic.LazicGym.model.Komentar;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.enumeration.StatusKomentara;
import com.ognjenlazic.LazicGym.service.KomentarService;
import com.ognjenlazic.LazicGym.service.KorisnikService;
import com.ognjenlazic.LazicGym.service.TerminService;
import com.ognjenlazic.LazicGym.service.TreningService;

@Controller
@RequestMapping(value = "/komentar")
public class KomentarController implements ServletContextAware{
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private TreningService treningService;
	@Autowired
	private TerminService terminService;
	@Autowired
	private KomentarService komentarService;

	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) Integer id, 
			HttpSession session)  throws IOException {
		
		Trening trening = treningService.findOne(id); 		
		ModelAndView rezultat = new ModelAndView("noviKomentar");
		rezultat.addObject("trening", trening); 		
		
		return rezultat; 
		
	}
	
	/*
	@GetMapping(value="/sviKomentariAdmin")
	public ModelAndView sviKomentariAdmin(
			@RequestParam(required=false) Integer id, 
			HttpSession session)  throws IOException {
		
		//Trening trening = treningService.findOne(id);
		List<Komentar> komentari = komentarService.findAll();
		ModelAndView rezultat = new ModelAndView("komentariAdmin");
		rezultat.addObject("komentari", komentari); 		
		
		return rezultat; 
	}
	*/
	
	@GetMapping(value="/prihvati")
	public void prihvati(
			@RequestParam int id, 
			HttpSession session, HttpServletResponse response)  throws IOException {
		
		Komentar komentar = komentarService.findOne(id);
		komentar.setStatus(StatusKomentara.odobren);
		komentarService.update(komentar);
		
		Trening trening = komentar.getTrening();
		trening.setProsjecnaOcjena((trening.getProsjecnaOcjena() + komentar.getOcjena()) / 2);
		treningService.update(trening);
		
		// OVDJE URADITI PROMJENU OCJENE TRENINGA

		response.sendRedirect(bURL + "sviKomentariAdmin.html");
	}
	
	@GetMapping(value="/odbij")
	public void odbij(
			@RequestParam int id, 
			HttpSession session, HttpServletResponse response)  throws IOException {
		
		Komentar komentar = komentarService.findOne(id);
		komentar.setStatus(StatusKomentara.odbijen);
		komentarService.update(komentar);
		response.sendRedirect(bURL + "sviKomentariAdmin.html");
	}
	
	@PostMapping(value="/dodaj")
	public void dodaj(
			@RequestParam int ocjena, @RequestParam String komentar, @RequestParam (required = false) boolean anoniman, @RequestParam int treningId, 
			HttpSession session, HttpServletResponse response)  throws IOException {
		System.out.println("KOMENTAR");
		System.out.println(komentar);
		System.out.println(ocjena);
		System.out.println(anoniman);
		System.out.println(treningId);
		System.out.println("KOMENTAR");
		
		Korisnik korisnik = (Korisnik)session.getAttribute("korisnik");
		Trening trening = treningService.findOne(treningId);
		
		List<Komentar> komentari = komentarService.findAll();
		int noviId = 0;
		for(Komentar k: komentari){
			if(k.getId() >= noviId) {
				noviId = k.getId() + 1;
			}
		}
		LocalDateTime trenutniDatum = LocalDateTime.now();
		
		Komentar noviKomentar;
		
		if(anoniman) {
			noviKomentar = new Komentar(noviId, komentar, ocjena, trenutniDatum, null, trening, StatusKomentara.naCekanju, anoniman, true);
		} else {
			noviKomentar = new Komentar(noviId, komentar, ocjena, trenutniDatum, korisnik, trening, StatusKomentara.naCekanju, anoniman, true);
		}
		komentarService.save(noviKomentar);
		response.sendRedirect(bURL + "trening/details?id=" + treningId);
		
	}
	
	
	@SuppressWarnings("unchecked")
	@GetMapping(value="/sviKomentariAdmin")
	@ResponseBody
	public List<Komentar> sviKomentar(HttpSession session){
		List<Komentar> sviKomentari = komentarService.findAll();
		List<Komentar> komentariNaCekanju = new ArrayList<Komentar>();
		
		for (Komentar k: sviKomentari) {
			if(k.getStatus() == StatusKomentara.naCekanju) {
				komentariNaCekanju.add(k);
			}
		}
		
		return 	 komentariNaCekanju;
	}
}
