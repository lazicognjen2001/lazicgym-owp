package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.ClanskaKarta;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.service.SalaService;
import com.ognjenlazic.LazicGym.service.TerminService;

@Controller
@RequestMapping(value = "/sale")
public class SaleController implements ServletContextAware{
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	@Autowired
	private SalaService salaService;
	@Autowired
	private TerminService terminService;
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	@GetMapping
	public ModelAndView index( 
			@RequestParam(required=false) String oznaka, 
			@RequestParam(required=false) Integer kapacitet,
			HttpSession session)  throws IOException {
		
		List<Sala> sale = salaService.findAll(oznaka, kapacitet);
		ModelAndView rezultat = new ModelAndView("sale");
		rezultat.addObject("sale", sale); 		
		
		return rezultat; 
		
	}
	
	@GetMapping(value="/dodajSalu")
	@ResponseBody
	public ModelAndView dodajSalu( 
			HttpSession session)  throws IOException {
		
		ModelAndView rezultat = new ModelAndView("novaSalaAdmin");
		return rezultat; 
	}
	
	
	@PostMapping(value="/dodaj")
	public void dodaj(
			@RequestParam String oznakaSale, 
			@RequestParam int kapacitet, 
			HttpSession session, HttpServletResponse response)  throws IOException {

		List<Sala> sale = salaService.findAll();
		
		int noviId = 0;
		
		for(Sala sala: sale) {
			if(sala.getId() >= noviId) {
				noviId = sala.getId() + 1;
 			}
		}
		
		Sala novaSala = new Sala(oznakaSale, kapacitet, noviId, false, true);
		salaService.save(novaSala);
		response.sendRedirect(bURL + "sale");

		
	}
	
	@PostMapping(value="/izmjenaSale")
	public void izmjeniSalu(
			@RequestParam int id, 
			@RequestParam String oznaka, 
			@RequestParam int kapacitet, 
			HttpSession session, HttpServletResponse response)  throws IOException {

		Sala sala = salaService.findOne(id);
		sala.setOznaka(oznaka);
		sala.setKapacitet(kapacitet);
		salaService.update(sala);
		response.sendRedirect(bURL + "sale");

		
	}
	
	@GetMapping(value="/izmjena")
	public ModelAndView izmejan(
			@RequestParam int id,  
			HttpSession session, HttpServletResponse response)  throws IOException {

		Sala sala = salaService.findOne(id);

		ModelAndView rezultat = new ModelAndView("izmjenaSale");
		rezultat.addObject("sala", sala);
		return rezultat;

		
	}
	
	@GetMapping(value="/izbrisi")
	public void izbrisi(
			@RequestParam int id, 
			HttpSession session, HttpServletResponse response)  throws IOException {
		
		boolean imaTerminUTojSali = false;
		List<Termin> termini = terminService.findAll();
		for (Termin t: termini) {
			if (t.getSala().getId() == id) {
				imaTerminUTojSali = true;
			}
		}

		
		if (!imaTerminUTojSali) {
			Sala sala = salaService.findOne(id);
			sala.setAktivan(false);
			salaService.update(sala);
		} else {
			System.out.println("Ima termin u toj sali nije moguce obrisati salu");
		}
		
		
		
		response.sendRedirect(bURL + "sale");

		
	}
}
