package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.Zelja;
import com.ognjenlazic.LazicGym.model.enumeration.Uloga;
import com.ognjenlazic.LazicGym.service.ListaZeljaService;
import com.ognjenlazic.LazicGym.service.TreningService;

@Controller
@RequestMapping(value = "/listaZelja")
public class ListaZeljaController implements ServletContextAware{

	@Autowired
	private ServletContext servletContext;
	private  String bURL;
	
	@Autowired
	private ListaZeljaService listaZeljaService;
	@Autowired
	private TreningService treningService;
	
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		// TODO Auto-generated method stub
		this.servletContext = servletContext;
	}
	
	@GetMapping
	public ModelAndView index( 
			HttpSession session)  throws IOException {
		
		List<Zelja> zelje = listaZeljaService.findAll(); 		
		ModelAndView rezultat = new ModelAndView("listaZelja");
		rezultat.addObject("zelje", zelje); 		
		
		return rezultat; 
		
	}
	
	@GetMapping(value="/dodaj")
	public void podesavanja(@RequestParam(required = true) int id, HttpSession session, HttpServletResponse response) throws IOException {
		
		List<Zelja> zelje = listaZeljaService.findAll();
		Trening trening = treningService.findOne(id);
		
		int noviId = 0;
		for (int i = 0; i < zelje.size(); i++) {
			if (noviId <= zelje.get(i).getId()) {
				noviId = zelje.get(i).getId() + 1;
			}
		}
		
		Korisnik trenutniKorisnik = (Korisnik)session.getAttribute("korisnik");
		
		Zelja novaZelja = new Zelja(noviId, trenutniKorisnik, trening, true);
		listaZeljaService.save(novaZelja);
				
		ModelAndView rezultat = new ModelAndView("listaZelja");		
		//return rezultat;
		response.sendRedirect("http://localhost:8080/LazicGym/listaZelja");
	}
	
	@GetMapping(value="/ukloni")
	public void ukloni(@RequestParam(required = true) int id, HttpSession session, HttpServletResponse response) throws IOException {

		Zelja zelja = listaZeljaService.findOne(id);
		zelja.setAktivan(false);
		listaZeljaService.update(zelja);
		ModelAndView rezultat = new ModelAndView("listaZelja");		
		//return rezultat;
		response.sendRedirect("http://localhost:8080/LazicGym/listaZelja");
	}

}
