package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.Izvjestaj;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.service.TerminService;

@Controller
@RequestMapping(value = "/izvjestaj")
public class IzvjestajController implements ServletContextAware{
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	@Autowired
	private TerminService terminService;
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	@GetMapping
	public ModelAndView index(

			HttpSession session)  throws IOException {
		
		ModelAndView rezultat = new ModelAndView("kreirajIzvjestaj"); // naziv template-a 
		return rezultat; 
		
	}
	
	@PostMapping(value="/kreiraj")
	public ModelAndView dodaj(
			@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumOd, 
			@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumDo, 
			HttpSession session, HttpServletResponse response)  throws IOException {
		
		LocalDateTime od = datumOd.atStartOfDay();
		LocalDateTime doo = datumDo.atStartOfDay();
		System.out.println("OD: " + od);
		System.out.println("DO: " + doo);
		List<Izvjestaj> izvjestaji = terminService.getIzvjestaj(od, doo);
		int ukupnaCijena = 0;
		for (Izvjestaj i : izvjestaji) {
			System.out.println("IZVJESTAJ: " + i.getNaziv() + " " + i.getUkupnaCijena());
			ukupnaCijena = ukupnaCijena + i.getUkupnaCijena();
		}
		System.out.println("IZVJESTAJ: " + izvjestaji.size());
		System.out.println("CIJENA: " + ukupnaCijena);
		ModelAndView rezultat = new ModelAndView("izvjestaji"); 
		rezultat.addObject("ukupnaCijena", ukupnaCijena);
		rezultat.addObject("izvjestaji", izvjestaji);
		return rezultat; 

	}
}
