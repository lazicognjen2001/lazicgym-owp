package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.ClanskaKarta;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.SpecijalniDatum;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.Zelja;
import com.ognjenlazic.LazicGym.service.ClanskaKartaService;
import com.ognjenlazic.LazicGym.service.SpecijalniDatumService;
import com.ognjenlazic.LazicGym.service.TerminService;

@Controller
@RequestMapping(value = "/korpa")
public class KkorpaController implements ServletContextAware{
	@Autowired
	private ServletContext servletContext;
	private  String bURL;
	
	@Autowired
	private TerminService terminService;
	@Autowired
	private SpecijalniDatumService specijalniDatumService;
	@Autowired
	private ClanskaKartaService clanskaKartaService;
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	}
	
	@GetMapping(value="/dodatiTermini")
	@ResponseBody
	public ModelAndView dodatiTermini( 
			HttpSession session)  throws IOException {
		
		List<SpecijalniDatum> datumi = specijalniDatumService.findAll();
		LocalDateTime now = LocalDateTime.now();
		Korisnik korisnik = (Korisnik)session.getAttribute("korisnik");
		ClanskaKarta karta;
		
		try {
			karta = clanskaKartaService.findOne(korisnik.getId());
		} catch(Error e) {
			karta = null;
		}
		
		SpecijalniDatum specijalniDatum = null;
		boolean imaDatum = false;
		
		for (SpecijalniDatum datum: datumi) {
			
			if(datum.getDatum().toLocalDate().isEqual(now.toLocalDate()) ) {
				System.out.println("DATUM " + datum.getDatum() + " JE U SPECIJALNIM DATUMIMA");
				specijalniDatum = datum;
				imaDatum = true;
				break;
			}
		}
		
		List<Termin> terminiUKorpi = (List<Termin>)session.getAttribute("korpa");
		float cijena = 0;
		for (int i = 0; i < terminiUKorpi.size(); i++) {
			cijena = cijena + terminiUKorpi.get(i).getTrening().getCijena();
		}
		float popust = 0;
		if(specijalniDatum != null) {
			popust = (float)specijalniDatum.getPopust()/100;
		}
		
		if(imaDatum) {
			float minus = cijena * popust;
			cijena = cijena - minus;
		}
		
		ModelAndView rezultat = new ModelAndView("korpa");	
		rezultat.addObject("korpa", terminiUKorpi);
		rezultat.addObject("karta", karta);
		rezultat.addObject("cijena", cijena);
		rezultat.addObject("specijalniDatum", specijalniDatum);
		return rezultat; 
		
	}
	
	@GetMapping(value="/dodaj")
	public void dodaj(@RequestParam(required = true) int id, HttpSession session, HttpServletResponse response) throws IOException {
		
		Termin termin = terminService.findOne(id);
		Korisnik trenutniKorisnik = (Korisnik)session.getAttribute("korisnik");

		termin.setKorisnik(trenutniKorisnik);
	
		List<Termin> terminiUKorpi = (List<Termin>)session.getAttribute("korpa");
		terminiUKorpi.add(termin);
		session.setAttribute("korpa", terminiUKorpi);
				
		ModelAndView rezultat = new ModelAndView("slobodniTermini");		
		//return rezultat;
		response.sendRedirect("http://localhost:8080/LazicGym/trening");
	}
	
	@GetMapping(value="/ukloni")
	public void ukloni(@RequestParam(required = true) int id, HttpSession session, HttpServletResponse response) throws IOException {
	
		List<Termin> terminiUKorpi = (List<Termin>)session.getAttribute("korpa");
		System.out.println(terminiUKorpi.size());
		
		for (int i = 0; i < terminiUKorpi.size(); i++) {
			if(terminiUKorpi.get(i).getId() == id ) {
				terminiUKorpi.remove(i);
				System.out.println("BRISANJE");
			}
		}
		/*
		for (Termin terminBrisanje: terminiUKorpi) {
			if(terminBrisanje.getId() == id) {
				terminiUKorpi.remove(terminBrisanje);
				System.out.println("BRISANJE");
			}
			
		}
		*/
	
		
		
		session.setAttribute("korpa", terminiUKorpi);
				
		ModelAndView rezultat = new ModelAndView("slobodniTermini");		
		//return rezultat;
		response.sendRedirect("http://localhost:8080/LazicGym/trening");
	}
	
	@GetMapping(value="/kupi")
	public void kupi( @RequestParam (required = false) boolean iskoristiClanskuKartu, @RequestParam (required = true) float cijena, @RequestParam (required = true) int clanskaKartaId,  HttpSession session, HttpServletResponse response) throws IOException {
	
		Korisnik trenutniKorisnik = (Korisnik)session.getAttribute("korisnik");
		ClanskaKarta karta = clanskaKartaService.findOne(clanskaKartaId);
		
		System.out.println("HOCE DA ISKORISTI: " + iskoristiClanskuKartu);
		System.out.println("HOCE DA ISKORISTI: " + cijena);
		if(iskoristiClanskuKartu) {
			float brojDodatinBodova = (float)cijena / 500;
			karta.setBrojPoena((int)(karta.getBrojPoena() + brojDodatinBodova));
		}
		
		List<Termin> terminiUKorpi = (List<Termin>)session.getAttribute("korpa");
		System.out.println(terminiUKorpi.size());
		
		for (Termin termin: terminiUKorpi) {
			termin.setKorisnik(trenutniKorisnik);
			terminService.update(termin);
			System.out.println("SACUVAN TERMIN: " + termin.getId() + " " + termin.getKorisnik().getId());
		}

		
		terminiUKorpi = new ArrayList<Termin>();		
		session.setAttribute("korpa", terminiUKorpi);
				
		ModelAndView rezultat = new ModelAndView("slobodniTermini");		
		//return rezultat;
		response.sendRedirect("http://localhost:8080/LazicGym/trening");
	}
}
