package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.service.KorisnikService;
import com.ognjenlazic.LazicGym.service.SalaService;
import com.ognjenlazic.LazicGym.service.TerminService;
import com.ognjenlazic.LazicGym.service.TreningService;

@Controller
@RequestMapping(value = "/termin")
public class TerminiController implements ServletContextAware {
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private TreningService treningService;
	@Autowired
	private TerminService terminService;
	@Autowired
	private SalaService salaService;
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	
	
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) String naziv, 
			@RequestParam(required=false) String trener, 
			@RequestParam(required=false) Integer cijenaOd, 
			@RequestParam(required=false) Integer cijenaDo, 
			@RequestParam(required=false) String vrstaTreninga, 
			@RequestParam(required=false) String nivoTreninga, 
			@RequestParam(required=false) Integer trajanjeOd, 
			@RequestParam(required=false) Integer trajanjeDo, 
			@RequestParam(required=false) String sortiranjeOcjena,
			HttpSession session)  throws IOException {
		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		
		List<Trening> trening = treningService.find(naziv, trener, cijenaOd, cijenaDo, vrstaTreninga, nivoTreninga, trajanjeOd, trajanjeDo, sortiranjeOcjena);		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("pocetniEkran"); // naziv template-a
		rezultat.addObject("treninzi", trening); // podatak koji se šalje template-u
		
		return rezultat; 
		
	}
	
	@GetMapping(value="/details")
	@ResponseBody
	public ModelAndView details(@RequestParam int id) {	
		Trening trening = treningService.findOne(id);
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("trening"); // naziv template-a
		rezultat.addObject("trening", trening); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping(value="/sviTerminiGost")
	@ResponseBody
	public ModelAndView treninziGost(
		@RequestParam(required=false) Integer salaId, 
		@RequestParam(required=false) Integer treningId,
		@RequestParam(required=false) Integer clanId, 
		// Dodati datum
		
		HttpSession session)  throws IOException {
	//ako je input tipa text i ništa se ne unese 
	//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
	
	List<Termin> termini = terminService.findAll();
	
	ModelAndView rezultat = new ModelAndView("sviTerminiGost"); // naziv template-a
	rezultat.addObject("termini", termini); // podatak koji se šalje template-u
	
	for(Termin termi : termini) {
		System.out.println("TERMIN:"+ termi.getId());
	}
	
	return rezultat; 
}
	
	@GetMapping(value="/sviTermini")
	@ResponseBody
	public ModelAndView sviTermini(
			@RequestParam(required=false) @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumOd, 
			@RequestParam(required=false) @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumDo,
		// Dodati datum
		
		HttpSession session)  throws IOException {
		List<Termin> termini ;
		if(datumOd == null || datumOd == null) {
			 termini = terminService.findAll();
			
		} else {
			LocalDateTime od = datumOd.atStartOfDay();
			LocalDateTime doo = datumDo.atStartOfDay();
			 termini = terminService.findAll(od, doo);
		
		}
		
	List<Termin> terminiUkorpi = (List<Termin>)session.getAttribute("korpa");
	List<Termin> dostupniTermini = new ArrayList<Termin>();
	
	for (Termin t1: termini) {
		boolean imaTermin = false;
		for (Termin t2: terminiUkorpi) {
			if(t1.getId() == t2.getId()) {
				imaTermin = true;
			}
		}
		if(!imaTermin) {
			dostupniTermini.add(t1);
			
		}
	}
	
	
	ModelAndView rezultat = new ModelAndView("slobodniTermini"); // naziv template-a
	rezultat.addObject("slobodniTermini", dostupniTermini); // podatak koji se šalje template-u
	
	for(Termin termi : termini) {
		System.out.println("TERMIN:"+ termi.getId());
	}
	
	return rezultat; 
	
}
	
	@GetMapping(value="/terminiAdmin")
	@ResponseBody
	public ModelAndView terminiAdmin(
			@RequestParam(required=false) @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumOd, 
			@RequestParam(required=false) @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumDo,
			HttpSession session) {	
		
		
		
		if(datumOd == null || datumOd == null) {
			List<Termin> termini = terminService.findAll();
			ModelAndView rezultat = new ModelAndView("terminiAdmin"); 
			rezultat.addObject("termini", termini); 
			return rezultat;
		} else {
			LocalDateTime od = datumOd.atStartOfDay();
			LocalDateTime doo = datumDo.atStartOfDay();
			List<Termin> termini = terminService.findAll(od, doo);
			ModelAndView rezultat = new ModelAndView("terminiAdmin"); 
			rezultat.addObject("termini", termini); 
			return rezultat;
		}
		
		
		 // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping(value="/ukloniTermin")
	public void odblokiraj(@RequestParam int id, HttpSession session, HttpServletResponse response) throws IOException {
		
		Termin termin = terminService.findOne(id);
		termin.setAktivan(false);
		terminService.update(termin);
		
		List<Korisnik> koriscnici = korisnikService.findAll();
		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", koriscnici);
		
		response.sendRedirect(bURL+ "termin/terminiAdmin");
	}
	
	@GetMapping(value="/dodajTermin")
	@ResponseBody
	public ModelAndView dodajtermin(HttpSession session, HttpServletResponse response) throws IOException {
		
		List<Sala> sale = salaService.findAll();
		List<Trening> treninzi = treningService.findAll();
		
		ModelAndView rezultat = new ModelAndView("noviterminAdmin");
		rezultat.addObject("sale", sale);
		rezultat.addObject("treninzi", treninzi);
		return rezultat;
	}
	
	
	
	@PostMapping(value="/dodaj")
	public void dodaj(
			@RequestParam int salaId, 
			@RequestParam int treningId, 
			@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datum, 
			@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.TIME) LocalTime vrijeme, 
			HttpSession session, HttpServletResponse response)  throws IOException {
		
		System.out.println(salaId);
		System.out.println(treningId);
		System.out.println(datum);
		System.out.println(vrijeme);
		
		LocalDateTime datumIVrijeme = LocalDateTime.of(datum, vrijeme);
		Sala sala = salaService.findOne(salaId);
		Trening trening = treningService.findOne(treningId);
		List<Termin> sviTermini = terminService.findAll();
		
		
		int noviId = 0;
		for (Termin t: sviTermini) {
			if(t.getId() >= noviId) {
				noviId = t.getId() + 1; 
			}
		}
		
		Termin noviTermin = new Termin(noviId, sala, trening, null, datumIVrijeme, false, true);
		
		

		
		// Algoritam za provjeru preklapanja datuma
		
		boolean neMozeSeRezervisati = false; 
		LocalDateTime datumNovogTermina = noviTermin.getDatumIVrijemeTreninga();
		LocalDateTime zavrsavanjeNovogTreninga = datumNovogTermina.plusMinutes(noviTermin.getTrening().getTrajanjeTreninga());

		int brojTerminaUSaliUTomVremenu = 0;
		for (Termin t : sviTermini) {
			
			
			LocalDateTime datumNadjenogTermina = t.getDatumIVrijemeTreninga();
			LocalDateTime zavrsavanjeNadjenogTreninga = datumNadjenogTermina.plusMinutes(t.getTrening().getTrajanjeTreninga());
			
			if(t.getSala().getId() == sala.getId()) {
				
				
				if(datumNovogTermina.toLocalDate().isEqual(datumNadjenogTermina.toLocalDate())) {
					
					if(
							datumNovogTermina.toLocalTime().equals(datumNadjenogTermina.toLocalTime())
							) {
						brojTerminaUSaliUTomVremenu ++;
					} else if (
							datumNovogTermina.toLocalTime().isAfter(datumNadjenogTermina.toLocalTime()) 
							&& datumNovogTermina.toLocalTime().isBefore(zavrsavanjeNadjenogTreninga.toLocalTime())
							){
						brojTerminaUSaliUTomVremenu ++;
					} else if(zavrsavanjeNovogTreninga.toLocalTime().isAfter(datumNadjenogTermina.toLocalTime()) 
							&& zavrsavanjeNovogTreninga.toLocalTime().isBefore(zavrsavanjeNadjenogTreninga.toLocalTime())
							) {
						brojTerminaUSaliUTomVremenu ++;
					} else if(datumNovogTermina.toLocalTime().isAfter(datumNadjenogTermina.toLocalTime()) 
							&& zavrsavanjeNovogTreninga.toLocalTime().isBefore(zavrsavanjeNadjenogTreninga.toLocalTime())
							) {
						brojTerminaUSaliUTomVremenu ++;
					} else if(datumNovogTermina.toLocalTime().isBefore(datumNadjenogTermina.toLocalTime()) 
							&& zavrsavanjeNovogTreninga.toLocalTime().isAfter(zavrsavanjeNadjenogTreninga.toLocalTime())
							)
					{
						brojTerminaUSaliUTomVremenu ++;
					} 
					
					if(sala.getKapacitet() <= brojTerminaUSaliUTomVremenu) {
						neMozeSeRezervisati = true;
						break;
					}
					System.out.println("Isti Datum");

					
				}	
				System.out.println("Iste sale");
			}
			System.out.println(datumNovogTermina);
			System.out.println(datumNadjenogTermina);
			System.out.println(brojTerminaUSaliUTomVremenu);
			System.out.println(neMozeSeRezervisati);

		}
		
		
		if(neMozeSeRezervisati) {
			response.sendRedirect(bURL + "termin/terminiAdmin");
		} else  {
			terminService.save(noviTermin);			
			response.sendRedirect(bURL + "termin/terminiAdmin");
		}
		
		
		
	
	}
	
	@GetMapping(value="/otkazi")
	public void otkazi(@RequestParam int id, HttpSession session, HttpServletResponse response) throws IOException {
		
		Termin termin = terminService.findOne(id);
		termin.setKorisnik(null);
		terminService.update(termin);
		System.out.println("ZAVRSENA IZMJENA");
		List<Korisnik> koriscnici = korisnikService.findAll();
		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", koriscnici);
		
		response.sendRedirect(bURL+ "trening");
	}

}
