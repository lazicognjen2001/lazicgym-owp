package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.ClanskaKarta;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.TipTreninga;
import com.ognjenlazic.LazicGym.model.Zelja;
import com.ognjenlazic.LazicGym.service.ClanskaKartaService;

@Controller
@RequestMapping(value = "/clanskaKartica")
public class ClanskaKarticaController implements ServletContextAware{

	@Autowired
	private ServletContext servletContext;
	private  String bURL;
	
	@Autowired
	private ClanskaKartaService clanskaKartaService;
	
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	} 
	
	@GetMapping(value="/clanskaKartaKorisnik")
	@ResponseBody
	public ModelAndView index( 
			HttpSession session)  throws IOException {
		
		List<ClanskaKarta> karte = clanskaKartaService.findAll(); 		
		ModelAndView rezultat = new ModelAndView("clanskaKarticaKorisnik");
		rezultat.addObject("karte", karte); 		
		
		return rezultat; 
		
	}
	
	@GetMapping(value="/novaKartica")
	@ResponseBody
	public ModelAndView novaKartica( 
			HttpSession session)  throws IOException {
		
		List<ClanskaKarta> karte = clanskaKartaService.findAll();
		Korisnik korisnik = (Korisnik)session.getAttribute("korisnik");
		boolean korisnikImaKartu = false;
		int noviId = 0;
		for (ClanskaKarta karta: karte) {
			if(karta.getKorisnik().getId() == korisnik.getId()) {
				korisnikImaKartu = true;
			}
			
			if(noviId <= karta.getId()) {
				noviId = karta.getId() + 1;
			}
			
		}
		
		if(korisnikImaKartu) {
			ModelAndView rezultat = new ModelAndView("clanskaKarticaKorisnik");
			rezultat.addObject("karte", karte);
			System.out.println("KORISNIK VEĆ IMA KARTU");
			return rezultat;
		} else {
			
			ClanskaKarta karta = new ClanskaKarta(noviId, 0, 0, korisnik, false, false, true);
			clanskaKartaService.save(karta);
			ModelAndView rezultat = new ModelAndView("clanskaKarticaKorisnik");
			karte.add(karta);
			System.out.println("KORISNIK NIJE IMAO KARTU");
			rezultat.addObject("karte", karte); 
			return rezultat;
		} 
		
	}
	
	@GetMapping(value="/sveKartice")
	@ResponseBody
	public ModelAndView sveKartice( 
			HttpSession session)  throws IOException {
		
		List<ClanskaKarta> karte = clanskaKartaService.findAll();
		;
			ModelAndView rezultat = new ModelAndView("clanskeKarteAdmin");
			rezultat.addObject("karte", karte); 
			return rezultat;
		} 
	
	@GetMapping(value="/prihvati")
	public void prihvati(
			@RequestParam int id, 
			HttpSession session, HttpServletResponse response)  throws IOException {
		
		ClanskaKarta karta = clanskaKartaService.findOne(id);
		karta.setPrihvacen(true);
		karta.setBrojPoena(10);
		clanskaKartaService.update(karta);

		response.sendRedirect(bURL + "clanskaKartica/sveKartice");
	}
	
	@GetMapping(value="/odbij")
	public void odbij(
			@RequestParam int id, 
			HttpSession session, HttpServletResponse response)  throws IOException {
		
		ClanskaKarta karta = clanskaKartaService.findOne(id);
		karta.setPrihvacen(false);
		clanskaKartaService.update(karta);

		response.sendRedirect(bURL + "clanskaKartica/sveKartice");
	}
		
	}

