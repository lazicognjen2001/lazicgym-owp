package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.TipTreninga;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.service.TipTreningaService;

@Controller
@RequestMapping(value = "/tipoviTreninga")
public class TipoviTreningaController implements ServletContextAware{
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL;
	
	@Autowired
	private TipTreningaService tipTreningaService;
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) Integer id, 
			HttpSession session)  throws IOException {
		
		List<TipTreninga> tipoviTreninga = tipTreningaService.findAll();		
		ModelAndView rezultat = new ModelAndView("tipoviTreningaAdmin");
		rezultat.addObject("tipoviTreninga", tipoviTreninga); 		
		
		return rezultat; 
		
	}
	
	@GetMapping(value="/dodajTip")
	@ResponseBody
	public ModelAndView dodajtermin(HttpSession session, HttpServletResponse response) throws IOException {
		
		ModelAndView rezultat = new ModelAndView("noviTipAdmin");
		
		return rezultat;
	}
	
	@PostMapping(value="/dodaj")
	public void dodaj(
			@RequestParam String ime, 
			@RequestParam String opis, 
			
			HttpSession session, HttpServletResponse response)  throws IOException {
		
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		int noviId = 0;
		for(TipTreninga tip: tipovi) {
			if(tip.getId() >= noviId) {
				noviId = tip.getId() + 1;
			}
		}
		
		TipTreninga noviTip = new TipTreninga(ime, opis, noviId, true);
		tipTreningaService.save(noviTip);
		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		
		ModelAndView rezultat = new ModelAndView("pocetniEkran"); // naziv template-a
		
		//return rezultat; 
		response.sendRedirect(bURL + "tipoviTreninga");
	}
}
