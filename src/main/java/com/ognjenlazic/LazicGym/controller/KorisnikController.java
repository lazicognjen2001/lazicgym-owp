package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.Zelja;
import com.ognjenlazic.LazicGym.model.enumeration.Uloga;
import com.ognjenlazic.LazicGym.service.KorisnikService;
import com.ognjenlazic.LazicGym.service.TreningService;


@Controller
@RequestMapping(value = "/korisnici")
public class KorisnikController implements ServletContextAware {
	
	public static final String KORISNIK_KEY = "korisnik";
	public static final String KORPA_KEY = "korpa";

	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private TreningService treningService;
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@GetMapping(value = "/login")
	public void getLogin(@RequestParam(required = false) String email, @RequestParam(required = false) String lozinka,
			HttpSession session, HttpServletResponse response) throws IOException {
		postLogin(email, lozinka, session, response);
	}
	
	@PostMapping(value = "/login")
	@ResponseBody
	public void postLogin(@RequestParam(required = false) String email, @RequestParam(required = false) String sifra,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = korisnikService.findOne(email, sifra);
		String greska = "";
		if (korisnik == null || korisnik.isBlokiran() == true)
			greska = "neispravni kredencijali";

		

		if (session.getAttribute(KORISNIK_KEY) != null)
			greska = "korisnik je već prijavljen na sistem morate se prethodno odjaviti<br/>";

		if (!greska.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;
			out = response.getWriter();

			StringBuilder retVal = new StringBuilder();
			retVal.append("<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "	<meta charset=\"UTF-8\">\r\n"
					+ "	<base href=\"/LazicGym/\">	\r\n" + "	<title>Prijava korisnika</title>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"
					+ "</head>\r\n" + "<body>\r\n" + "	<ul>\r\n"
					+ "		<li><a href=\"registracija.html\">Registruj se</a></li>\r\n" + "	</ul>\r\n");
			if (!greska.equals(""))
				retVal.append("	<div>" + greska + "</div>\r\n");
			retVal.append("	<a href=\"index.html\">Povratak</a>\r\n" + "	<br/>\r\n" + "</body>\r\n" + "</html>");

			out.write(retVal.toString());
			return;
		}

		session.setAttribute(KORISNIK_KEY, korisnik);	
		
		
		List<Termin> korpa = new ArrayList<Termin>();
		session.setAttribute(KORPA_KEY, korpa);
		response.sendRedirect(bURL + "trening");
	}
	
	@PostMapping(value = "/registracija")
	public void registracija(@RequestParam(required = true) String email, @RequestParam(required = true) String lozinka,
			@RequestParam(required = true) String ime, @RequestParam(required = true) String prezime, @RequestParam(required = true) String datumRodjenja,
			@RequestParam(required = true) String adresa, @RequestParam(required = true) String brojTelefona, @RequestParam(required = true) String korisnickoIme,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		List<Korisnik> sviKorisnici = korisnikService.findAll();
		int noviId = 0;
		for (int i = 0; i < sviKorisnici.size(); i++) {
			if (noviId <= sviKorisnici.get(i).getId()) {
				noviId = sviKorisnici.get(i).getId() + 1;
			}
		}
		
		System.out.println(datumRodjenja);
		System.out.println(noviId);
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  

		Date datumRodjenjaDate = Date.valueOf(datumRodjenja);
		
		long brojTelefonaLong = Long.parseLong(brojTelefona);
				
		Korisnik korisnik = new Korisnik(noviId, korisnickoIme, lozinka, email, ime, prezime, datumRodjenjaDate, adresa, brojTelefonaLong, now, Uloga.clan, false, true);
		
		korisnikService.save(korisnik);
		session.setAttribute(KORISNIK_KEY, korisnik);
		
		response.sendRedirect(bURL + "trening");
	}
	
	@GetMapping(value="/odjava")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		// odjava	
		session.invalidate();
		
		response.sendRedirect(bURL);
	}
	
	@GetMapping(value="/podesavanja")
	public ModelAndView podesavanja(HttpSession session, HttpServletResponse response) throws IOException {
		
		ModelAndView rezultat = new ModelAndView("userSettings");		
		
		return rezultat;
	}

	
	
	@PostMapping(value="/podesavanjaFn")
	public ModelAndView podesavanja(@RequestParam(required = true) String email, @RequestParam(required = false) String prvaLozinka, @RequestParam(required = false) String drugaLozinka,
			@RequestParam(required = true) String ime, @RequestParam(required = true) String prezime, @RequestParam(required = true) String datumRodjenja,
			@RequestParam(required = true) String adresa, @RequestParam(required = true) String brojTelefona, @RequestParam(required = true) String korisnickoIme,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik trenutniKorisnik = (Korisnik)session.getAttribute(KORISNIK_KEY);
		
		System.out.println(prvaLozinka);
		System.out.println(drugaLozinka);
		
		if(prvaLozinka.equals(drugaLozinka)) {
			trenutniKorisnik.setLozinka(drugaLozinka);
		}


		Date datumRodjenjaDate = Date.valueOf(datumRodjenja);
		long brojTelefonaLong = Long.parseLong(brojTelefona);
				
		Korisnik korisnik = new Korisnik(trenutniKorisnik.getId(), korisnickoIme, trenutniKorisnik.getLozinka(), email, ime, prezime, datumRodjenjaDate, adresa, brojTelefonaLong, trenutniKorisnik.getDatumIVrijemeRegistracije(), Uloga.clan, false, true);
		
		korisnikService.update(korisnik);
		session.setAttribute(KORISNIK_KEY, korisnik);
		
				
		ModelAndView rezultat = new ModelAndView("userSettings");		
		
		return rezultat;
	}
	
	@GetMapping(value="/sviKorisnici")
	public ModelAndView sviKorisnici(
			@RequestParam(required=false) String ime, 
			@RequestParam(required=false) String prezime,
			@RequestParam(required=false) String email, 
			@RequestParam(required=false) String korisnickoIme,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		List<Korisnik> koriscnici = korisnikService.findAll(email, korisnickoIme, ime, prezime);
		
		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", koriscnici);
		return rezultat;
	}
	
	@GetMapping(value="/blokiraj")
	public void blokiraj(@RequestParam int id, HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnikZaBlokiranje = korisnikService.findOne(id);
		korisnikZaBlokiranje.setBlokiran(true);
		korisnikService.update(korisnikZaBlokiranje);
		
		List<Korisnik> koriscnici = korisnikService.findAll();
		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", koriscnici);
		response.sendRedirect(bURL+ "korisnici/sviKorisnici");
	}
	
	@GetMapping(value="/odblokiraj")
	public void odblokiraj(@RequestParam int id, HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnikZaBlokiranje = korisnikService.findOne(id);
		korisnikZaBlokiranje.setBlokiran(false);
		korisnikService.update(korisnikZaBlokiranje);
		
		List<Korisnik> koriscnici = korisnikService.findAll();
		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", koriscnici);
		response.sendRedirect(bURL+ "korisnici/sviKorisnici");
	}
	
	@GetMapping(value="/izmjena")
	public ModelAndView izmjena(@RequestParam int id, HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = korisnikService.findOne(id);

		ModelAndView rezultat = new ModelAndView("izmjenaKorisnikaAdmin");
		rezultat.addObject("korisnik", korisnik);
		return rezultat;
		//response.sendRedirect(bURL+ "korisnici/sviKorisnici");
	}
	
	@PostMapping(value="/izmjenaKorisnika")
	public void izmjenaKorisnika(@RequestParam(required = false) String prvaLozinka, @RequestParam(required = false) String drugaLozinka,
			@RequestParam(required = true) String uloga, @RequestParam(required = true) int id, 
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = korisnikService.findOne(id);
		
		System.out.println(prvaLozinka);
		System.out.println(drugaLozinka);
		korisnik.setUloga(Uloga.valueOf(uloga));
		if(prvaLozinka.equals(drugaLozinka)) {
			korisnik.setLozinka(drugaLozinka);
		}


		korisnikService.update(korisnik);
		response.sendRedirect(bURL+ "korisnici/sviKorisnici");
		
	}
	
}
