package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.model.SpecijalniDatum;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.service.SalaService;
import com.ognjenlazic.LazicGym.service.SpecijalniDatumService;

@Controller
@RequestMapping(value = "/specijalniDatum")
public class SpecijalniDatumController implements ServletContextAware{
	@Autowired
	private ServletContext servletContext;
	private  String bURL;
	@Autowired
	private SpecijalniDatumService specijalniDatumServiece;
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	}
	
	@GetMapping(value="/dodavanje")
	public ModelAndView podesavanja(HttpSession session, HttpServletResponse response) throws IOException {
		
		ModelAndView rezultat = new ModelAndView("dodavanjeSpecijalnogDatuma");		
		
		return rezultat;
	}
	
	@PostMapping(value="/dodaj")
	public void dodaj(
			@RequestParam int popust, 
			@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datum, 
			HttpSession session, HttpServletResponse response)  throws IOException {

		System.out.println(datum);
		System.out.println(popust);
		
		LocalDateTime datumIVrijeme = LocalDateTime.of(datum, LocalTime.now());


		List<SpecijalniDatum> datumi = specijalniDatumServiece.findAll();
		
		int noviId = 0;
		for (SpecijalniDatum d: datumi) {
			if(d.getId() >= noviId) {
				noviId = d.getId() + 1; 
			}
		}
		SpecijalniDatum noviDatum = new SpecijalniDatum(noviId, datumIVrijeme, popust, true);
		specijalniDatumServiece.save(noviDatum);
		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null		
		//return rezultat; 
		response.sendRedirect(bURL + "trening");
	}	
}
