package com.ognjenlazic.LazicGym.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ognjenlazic.LazicGym.model.Izvjestaj;
import com.ognjenlazic.LazicGym.model.Komentar;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.model.TipTreninga;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.model.enumeration.NivoTreninga;
import com.ognjenlazic.LazicGym.model.enumeration.VrstaTreninga;
import com.ognjenlazic.LazicGym.service.KomentarService;
import com.ognjenlazic.LazicGym.service.KorisnikService;
import com.ognjenlazic.LazicGym.service.TerminService;
import com.ognjenlazic.LazicGym.service.TipTreningaService;
import com.ognjenlazic.LazicGym.service.TreningService;

@Controller
@RequestMapping(value = "/trening")
public class TreningController implements ServletContextAware {
	
public static final String KORISNIK_KEY = "korisnik";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private TreningService treningService;
	@Autowired
	private TerminService terminService;
	@Autowired
	private KomentarService komentarService;
	@Autowired
	private TipTreningaService tipTreningaService;
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	
	
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) String naziv, 
			@RequestParam(required=false) String trener, 
			@RequestParam(required=false) Integer cijenaOd, 
			@RequestParam(required=false) Integer cijenaDo, 
			@RequestParam(required=false) String vrstaTreninga, 
			@RequestParam(required=false) String nivoTreninga, 
			@RequestParam(required=false) Integer trajanjeOd, 
			@RequestParam(required=false) Integer trajanjeDo, 
			@RequestParam(required=false) String sortiranjeOcjena,
			HttpSession session)  throws IOException {
		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		
		List<Trening> trening = treningService.find(naziv, trener, cijenaOd, cijenaDo, vrstaTreninga, nivoTreninga, trajanjeOd, trajanjeDo, sortiranjeOcjena);		
		
		ModelAndView rezultat = new ModelAndView("pocetniEkran"); // naziv template-a
		rezultat.addObject("treninzi", trening); // podatak koji se šalje template-u
		
		List<Izvjestaj> izvjestaji = terminService.getIzvjestaj(LocalDateTime.now(), LocalDateTime.of(2023, 2, 13, 15, 56));
		List<Termin> termini = terminService.findAll();
		
		Korisnik trenutniKorisnik = (Korisnik)session.getAttribute("korisnik");
		List<Termin> terminiFiltrirani = new ArrayList<Termin>();

		for (Termin termin : termini) {
			if(termin.getKorisnik() != null) {
				terminiFiltrirani.add(termin);
			}
		}
		
		for (Izvjestaj i : izvjestaji) {
			System.out.println("IZVJESTAJ: " + i.getNaziv() + " " + i.getUkupnaCijena());
		}
		
		
		rezultat.addObject("termini", terminiFiltrirani);
 
		return rezultat; 
		
	}
	
	@GetMapping(value="/details")
	@ResponseBody
	public ModelAndView details(@RequestParam int id) {	
		Trening trening = treningService.findOne(id);
		List<Komentar> komentari = komentarService.findAll();
		ModelAndView rezultat = new ModelAndView("trening"); // naziv template-a
		rezultat.addObject("trening", trening); // podatak koji se šalje template-u
		rezultat.addObject("komentari", komentari);

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping(value="/detailsGost")
	@ResponseBody
	public ModelAndView detailsGost(@RequestParam int id) {	
		Trening trening = treningService.findOne(id);
		List<Komentar> komentari = komentarService.findAll();
		ModelAndView rezultat = new ModelAndView("treningInfoGost"); // naziv template-a
		rezultat.addObject("trening", trening); // podatak koji se šalje template-u
		rezultat.addObject("komentari", komentari);
		return rezultat; 
	}
	
	
	@GetMapping(value="/add")
	@ResponseBody
	public ModelAndView create(HttpSession session, HttpServletResponse response){
		List<TipTreninga> tipoviTreninga = tipTreningaService.findAll();
		ModelAndView rezultat = new ModelAndView("dodavanjeTreninga"); // naziv template-a
		rezultat.addObject("tipoviTreninga", tipoviTreninga); // podatak koji se šalje template-u
		
		return rezultat; // stranica za dodavanje treninga
	}
	
	@SuppressWarnings("unused")
	@PostMapping(value="/add")
	public void create(@RequestParam String naziv, @RequestParam String trener,  
			@RequestParam String opis, @RequestParam int cijena, @RequestParam int tipTreningaId, @RequestParam String vrsta, @RequestParam String nivoTreninga, 
			@RequestParam int trajanjeTreninga, HttpServletResponse response) throws IOException {		
		TipTreninga tip = tipTreningaService.findOne(tipTreningaId);
		List<Trening> sviTreninzi = treningService.findAll();
		int noviId = 0;
		for (int i = 0; i < sviTreninzi.size(); i++) {
			if (noviId <= sviTreninzi.get(i).getId()) {
				noviId = sviTreninzi.get(i).getId() + 1;
			}
		}

		String urlSlike = "https://images.unsplash.com/photo-1581009137042-c552e485697a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80";
		// Izmjeniti da se unese url slike u tekst polje
		
		Trening trening = new Trening(noviId, naziv, trener, opis, cijena, VrstaTreninga.valueOf(vrsta), NivoTreninga.valueOf(nivoTreninga), tip, urlSlike, trajanjeTreninga, 0, true);

		int saved = treningService.save(trening);
		response.sendRedirect(bURL+"trening");
	}
	
	
	@PostMapping(value="/edit")
	public void Edit(@RequestParam int id, @RequestParam String naziv, @RequestParam String trener,  
			@RequestParam String opis, @RequestParam String slika, @RequestParam int cijena, @RequestParam String vrsta, @RequestParam String nivoTreninga, 
			@RequestParam int trajanjeTreninga, HttpServletResponse response) throws IOException {	
		
		Trening trening = treningService.findOne(id);
		if(trening != null) {
			if(naziv != null && !naziv.trim().equals(""))
				trening.setNaziv(naziv);
			if(trener != null && !trener.trim().equals(""))
				trening.setTrener(trener);
			if(opis != null && !opis.trim().equals(""))
				trening.setOpis(opis);
			if(slika != null && !slika.trim().equals(""))
				trening.setSlika(slika);
			if(cijena > 0)
				trening.setCijena(cijena);
			if(vrsta != null && !vrsta.trim().equals(""))
				trening.setVrsta(VrstaTreninga.valueOf(vrsta));
			if(nivoTreninga != null && !nivoTreninga.trim().equals(""))
				trening.setNivoTreninga(NivoTreninga.valueOf(nivoTreninga));
			if(trajanjeTreninga > 30)
				trening.setTrajanjeTreninga(trajanjeTreninga);	
		}
		int saved = treningService.update(trening);
		System.out.println("DA LI JE TRENING USPIO: " + saved);
		response.sendRedirect(bURL+"trening");
	}
	
	@GetMapping(value="/sviTreninziGost")
	@ResponseBody
	public ModelAndView treninziGost(@RequestParam(required=false) String naziv, 
		@RequestParam(required=false) String trener, 
		@RequestParam(required=false) Integer cijenaOd, 
		@RequestParam(required=false) Integer cijenaDo, 
		@RequestParam(required=false) String vrstaTreninga, 
		@RequestParam(required=false) String nivoTreninga, 
		@RequestParam(required=false) Integer trajanjeOd, 
		@RequestParam(required=false) Integer trajanjeDo, 
		@RequestParam(required=false) String sortiranjeOcjena,
		HttpSession session)  throws IOException {
	//ako je input tipa text i ništa se ne unese 
	//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
	if(naziv!=null && naziv.trim().equals(""))
		naziv=null;
	
	List<Trening> trening = treningService.find(naziv, trener, cijenaOd, cijenaDo, vrstaTreninga, nivoTreninga, trajanjeOd, trajanjeDo, sortiranjeOcjena);		
	// podaci sa nazivom template-a
	ModelAndView rezultat = new ModelAndView("sviTreninziGost"); // naziv template-a
	rezultat.addObject("treninzi", trening); // podatak koji se šalje template-u
	
	return rezultat; 
	
}
	
	@GetMapping(value="/sviTreninzi")
	@ResponseBody
	public ModelAndView treninzi(@RequestParam(required=false) String naziv, 
		@RequestParam(required=false) String trener, 
		@RequestParam(required=false) Integer cijenaOd, 
		@RequestParam(required=false) Integer cijenaDo, 
		@RequestParam(required=false) String vrstaTreninga, 
		@RequestParam(required=false) String nivoTreninga, 
		@RequestParam(required=false) Integer trajanjeOd, 
		@RequestParam(required=false) Integer trajanjeDo, 
		@RequestParam(required=false) String sortiranjeOcjena,
		HttpSession session)  throws IOException {
	//ako je input tipa text i ništa se ne unese 
	//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
	if(naziv!=null && naziv.trim().equals(""))
		naziv=null;
	
	List<Trening> trening = treningService.find(naziv, trener, cijenaOd, cijenaDo, vrstaTreninga, nivoTreninga, trajanjeOd, trajanjeDo, sortiranjeOcjena);		
	// podaci sa nazivom template-a
	ModelAndView rezultat = new ModelAndView("sviTreninzi"); // naziv template-a
	rezultat.addObject("sviTreninzi", trening); // podatak koji se šalje template-u
	
	return rezultat; 
	
}
	
	@GetMapping(value="/treninziAdmin")
	@ResponseBody
	public ModelAndView treninziAdmin(@RequestParam(required=false) String naziv, 
		@RequestParam(required=false) String trener, 
		@RequestParam(required=false) Integer cijenaOd, 
		@RequestParam(required=false) Integer cijenaDo, 
		@RequestParam(required=false) String vrstaTreninga, 
		@RequestParam(required=false) String nivoTreninga, 
		@RequestParam(required=false) Integer trajanjeOd, 
		@RequestParam(required=false) Integer trajanjeDo, 
		@RequestParam(required=false) String sortiranjeOcjena,
		HttpSession session)  throws IOException {
	//ako je input tipa text i ništa se ne unese 
	//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
	if(naziv!=null && naziv.trim().equals(""))
		naziv=null;
	
	List<Trening> trening = treningService.find(naziv, trener, cijenaOd, cijenaDo, vrstaTreninga, nivoTreninga, trajanjeOd, trajanjeDo, sortiranjeOcjena);		
	// podaci sa nazivom template-a
	ModelAndView rezultat = new ModelAndView("treninziAdmin"); // naziv template-a
	rezultat.addObject("treninzi", trening); // podatak koji se šalje template-u
	
	return rezultat; 
	
}
}
