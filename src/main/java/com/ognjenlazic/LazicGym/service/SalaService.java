package com.ognjenlazic.LazicGym.service;

import java.util.List;

import com.ognjenlazic.LazicGym.model.Sala;

public interface SalaService {
	public Sala findOne(int id);
	public List<Sala> findAll();
	public int save(Sala sala);
	public int update(Sala sala);
	public int delete(Sala sala);
	public List<Sala> findAll(String oznaka, Integer kapacitet);
}
