package com.ognjenlazic.LazicGym.service;

import java.util.List;

import com.ognjenlazic.LazicGym.model.Zelja;

public interface ListaZeljaService {
	public Zelja findOne(int id);
	public List<Zelja> findAll();
	public int save(Zelja zelja);
	public int update(Zelja zelja);
	public int delete(Zelja zelja);
}
