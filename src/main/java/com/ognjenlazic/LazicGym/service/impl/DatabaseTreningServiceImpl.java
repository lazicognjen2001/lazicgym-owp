package com.ognjenlazic.LazicGym.service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.TreningDAO;
import com.ognjenlazic.LazicGym.model.Trening;
import com.ognjenlazic.LazicGym.service.TreningService;

@Service
public class DatabaseTreningServiceImpl implements TreningService {
	
	@Autowired
	private TreningDAO treningDao;

	@Override
	public Trening findOne(int id) {
		return treningDao.findOne(id);
	}

	@Override
	public List<Trening> findAll() {
		return treningDao.findAll();
	}

	@Override
	public int save(Trening trening) {
		return treningDao.save(trening);
	}

	@Override
	public int update(Trening trening) {
		return treningDao.update(trening);
	}

	@Override
	public int delete(Trening trening) {
		return treningDao.delete(trening);
	}

	@Override
	public List<Trening> find(String naziv, String trener, Integer cijenaOd, Integer cijenaDo, String vrstaTreninga,
			String nivoTreninga, Integer trejenjeOd, Integer trajanjeDo, String sortiranjeOcjena) {
		return treningDao.find(naziv, trener, cijenaOd, cijenaDo, vrstaTreninga, nivoTreninga, trejenjeOd, trajanjeDo, sortiranjeOcjena);
	}

}
