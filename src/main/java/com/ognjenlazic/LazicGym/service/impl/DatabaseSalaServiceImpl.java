package com.ognjenlazic.LazicGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.SalaDAO;
import com.ognjenlazic.LazicGym.dao.TerminDAO;
import com.ognjenlazic.LazicGym.model.Sala;
import com.ognjenlazic.LazicGym.service.SalaService;
@Service
public class DatabaseSalaServiceImpl implements SalaService {

	
	@Autowired
	private SalaDAO salaDao;
	
	@Override
	public Sala findOne(int id) {
		// TODO Auto-generated method stub
		return salaDao.findOne(id);
	}

	@Override
	public List<Sala> findAll() {
		// TODO Auto-generated method stub
		return salaDao.findAll();
	}

	@Override
	public int save(Sala sala) {
		// TODO Auto-generated method stub
		return salaDao.save(sala);
	}

	@Override
	public int update(Sala sala) {
		// TODO Auto-generated method stub
		return salaDao.update(sala);
	}

	@Override
	public int delete(Sala sala) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Sala> findAll(String oznaka, Integer kapacitet) {
		// TODO Auto-generated method stub
		return salaDao.findAll(oznaka, kapacitet);
		}

}
