package com.ognjenlazic.LazicGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.KomentarDAO;
import com.ognjenlazic.LazicGym.model.Komentar;
import com.ognjenlazic.LazicGym.service.KomentarService;
@Service
public class DatabaseKomentarServiceImpl implements KomentarService{
	
	@Autowired
	private KomentarDAO komentarDao;

	@Override
	public Komentar findOne(int id) {
		// TODO Auto-generated method stub
		return komentarDao.findOne(id);
	}

	@Override
	public List<Komentar> findAll() {
		// TODO Auto-generated method stub
		return komentarDao.findAll();
	}

	@Override
	public int save(Komentar komentar) {
		// TODO Auto-generated method stub
		return komentarDao.save(komentar);
	}

	@Override
	public int update(Komentar komentar) {
		// TODO Auto-generated method stub
		return komentarDao.update(komentar);
	}

	@Override
	public int delete(Komentar komentar) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Komentar findOne(String email, String sifra) {
		// TODO Auto-generated method stub
		return null;
	}

}
