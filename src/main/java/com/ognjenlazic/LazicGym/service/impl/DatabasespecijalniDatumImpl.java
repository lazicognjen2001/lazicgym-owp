package com.ognjenlazic.LazicGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.SpecijalniDatumDAO;
import com.ognjenlazic.LazicGym.model.SpecijalniDatum;
import com.ognjenlazic.LazicGym.service.SpecijalniDatumService;

@Service
public class DatabasespecijalniDatumImpl implements SpecijalniDatumService{
	@Autowired
	private SpecijalniDatumDAO specijalniDatumDao;

	@Override
	public SpecijalniDatum findOne(int id) {
		// TODO Auto-generated method stub
		return specijalniDatumDao.findOne(id);
	}

	@Override
	public List<SpecijalniDatum> findAll() {
		// TODO Auto-generated method stub
		return specijalniDatumDao.findAll();
	}

	@Override
	public int save(SpecijalniDatum specijalniDatum) {
		// TODO Auto-generated method stub
		return specijalniDatumDao.save(specijalniDatum);
	}

	@Override
	public int update(SpecijalniDatum specijalniDatum) {
		// TODO Auto-generated method stub
		return specijalniDatumDao.update(specijalniDatum);
	}

	@Override
	public int delete(SpecijalniDatum specijalniDatum) {
		// TODO Auto-generated method stub
		return specijalniDatumDao.delete(specijalniDatum);
	}
}
