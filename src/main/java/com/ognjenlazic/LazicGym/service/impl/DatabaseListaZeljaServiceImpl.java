package com.ognjenlazic.LazicGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.ListaZeljaDAO;
import com.ognjenlazic.LazicGym.model.Zelja;
import com.ognjenlazic.LazicGym.service.ListaZeljaService;

@Service
public class DatabaseListaZeljaServiceImpl implements ListaZeljaService{
	@Autowired
	private ListaZeljaDAO listaZeljaDao;

	@Override
	public Zelja findOne(int id) {
		// TODO Auto-generated method stub
		return listaZeljaDao.findOne(id);
	}

	@Override
	public List<Zelja> findAll() {
		// TODO Auto-generated method stub
		return listaZeljaDao.findAll();
	}

	@Override
	public int save(Zelja zelja) {
		// TODO Auto-generated method stub
		return listaZeljaDao.save(zelja);
	}

	@Override
	public int update(Zelja zelja) {
		// TODO Auto-generated method stub
		return listaZeljaDao.update(zelja);
	}

	@Override
	public int delete(Zelja zelja) {
		// TODO Auto-generated method stub
		return listaZeljaDao.delete(zelja);
	}
}
