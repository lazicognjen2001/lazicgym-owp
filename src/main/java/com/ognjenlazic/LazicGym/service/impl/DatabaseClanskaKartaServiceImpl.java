package com.ognjenlazic.LazicGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.ClanskaKartaDAO;
import com.ognjenlazic.LazicGym.dao.KomentarDAO;
import com.ognjenlazic.LazicGym.model.ClanskaKarta;
import com.ognjenlazic.LazicGym.service.ClanskaKartaService;
@Service
public class DatabaseClanskaKartaServiceImpl implements ClanskaKartaService {
	
	@Autowired
	private ClanskaKartaDAO clanskaKarticaDao;

	@Override
	public ClanskaKarta findOne(int id) {
		// TODO Auto-generated method stub
		return clanskaKarticaDao.findOne(id);
		}

	@Override
	public List<ClanskaKarta> findAll() {
		// TODO Auto-generated method stub
		return clanskaKarticaDao.findAll();
	}

	@Override
	public int save(ClanskaKarta clanskaKarta) {
		// TODO Auto-generated method stub
		return clanskaKarticaDao.save(clanskaKarta);
	}

	@Override
	public int update(ClanskaKarta clanskaKarta) {
		// TODO Auto-generated method stub
		return clanskaKarticaDao.update(clanskaKarta);
	}

	@Override
	public int delete(ClanskaKarta clanskaKarta) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<ClanskaKarta> find(String naziv, String trener, Integer cijenaOd, Integer cijenaDo,
			String vrstaTreninga, String nivoTreninga, Integer trejenjeOd, Integer trajanjeDo,
			String sortiranjeOcjena) {
		// TODO Auto-generated method stub
		return null;
	}

}
