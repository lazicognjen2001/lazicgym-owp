package com.ognjenlazic.LazicGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.TerminDAO;
import com.ognjenlazic.LazicGym.dao.TipTreningaDAO;
import com.ognjenlazic.LazicGym.model.TipTreninga;
import com.ognjenlazic.LazicGym.service.TipTreningaService;
@Service
public class DatabaseTipTreningaServiceImpl implements TipTreningaService {

	@Autowired
	private TipTreningaDAO tipTreningaDao;
	
	@Override
	public TipTreninga findOne(int id) {
		// TODO Auto-generated method stub
		return tipTreningaDao.findOne(id);
	}

	@Override
	public List<TipTreninga> findAll() {
		// TODO Auto-generated method stub
		return tipTreningaDao.findAll();
	}

	@Override
	public int save(TipTreninga tipTreninga) {
		// TODO Auto-generated method stub
		return tipTreningaDao.save(tipTreninga);
	}

	@Override
	public int update(TipTreninga tipTreninga) {
		// TODO Auto-generated method stub
		return tipTreningaDao.update(tipTreninga);
	}

	@Override
	public int delete(TipTreninga tipTreninga) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public TipTreninga findOne(String email, String sifra) {
		// TODO Auto-generated method stub
		return null;
	}

}
