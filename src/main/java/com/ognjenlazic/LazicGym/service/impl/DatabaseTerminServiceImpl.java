package com.ognjenlazic.LazicGym.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.TerminDAO;
import com.ognjenlazic.LazicGym.model.Izvjestaj;
import com.ognjenlazic.LazicGym.model.Termin;
import com.ognjenlazic.LazicGym.service.TerminService;
@Service
public class DatabaseTerminServiceImpl implements TerminService{

	@Autowired
	private TerminDAO terminDao;
	
	@Override
	public Termin findOne(int id) {
		return terminDao.findOne(id);
	}

	@Override
	public List<Termin> findAll() {
		return terminDao.findAll();
	}

	@Override
	public int save(Termin termin) {
		// TODO Auto-generated method stub
		return terminDao.save(termin);
	}

	@Override
	public int update(Termin termin) {
		// TODO Auto-generated method stub
		return terminDao.update(termin);
	}

	@Override
	public int delete(Termin termin) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Termin findOne(String email, String sifra) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Izvjestaj> getIzvjestaj(LocalDateTime datumOd, LocalDateTime datumDo) {
		// TODO Auto-generated method stub
		return terminDao.getIzvjestaj(datumOd, datumDo);
	}

	@Override
	public List<Termin> findAll(LocalDateTime datumOd, LocalDateTime datumDo) {
		// TODO Auto-generated method stub
		return terminDao.findAll(datumOd, datumDo);
	}

}
