package com.ognjenlazic.LazicGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ognjenlazic.LazicGym.dao.KorisnikDAO;
import com.ognjenlazic.LazicGym.model.Korisnik;
import com.ognjenlazic.LazicGym.service.KorisnikService;

@Service
public class DatabaseKorisnikServiceImpl implements KorisnikService{
	
	@Autowired
	private KorisnikDAO korisnikDao;

	@Override
	public Korisnik findOne(int id) {
		return korisnikDao.findOne(id);
	}

	@Override
	public List<Korisnik> findAll() {
		return korisnikDao.findAll();
	}

	@Override
	public int save(Korisnik korisnik) {
		return korisnikDao.save(korisnik);
	}

	@Override
	public int update(Korisnik korisnik) {
		return korisnikDao.update(korisnik);
	}

	@Override
	public int delete(Korisnik korisnik) {
		return korisnikDao.delete(korisnik);
	}

	@Override
	public Korisnik findOne(String lozinka, String sifra) {
		return korisnikDao.findOne(lozinka, sifra);
	}

	@Override
	public List<Korisnik> findAll(String email, String korisnickoIme, String ime, String prezime) {
		// TODO Auto-generated method stub
		return korisnikDao.findAll(email, korisnickoIme, ime, prezime);
	}

}
