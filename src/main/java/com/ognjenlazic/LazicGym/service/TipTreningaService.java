package com.ognjenlazic.LazicGym.service;

import java.util.List;

import com.ognjenlazic.LazicGym.model.TipTreninga;

public interface TipTreningaService {
	public TipTreninga findOne(int id);
	public List<TipTreninga> findAll();
	public int save(TipTreninga tipTreninga);
	public int update(TipTreninga tipTreninga);
	public int delete(TipTreninga tipTreninga);
	public TipTreninga findOne(String email, String sifra);
}
