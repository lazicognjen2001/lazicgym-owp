package com.ognjenlazic.LazicGym.service;

import java.util.List;

import com.ognjenlazic.LazicGym.model.ClanskaKarta;

public interface ClanskaKartaService {
	public ClanskaKarta findOne(int id);
	public List<ClanskaKarta> findAll();
	public int save(ClanskaKarta clanskaKarta);
	public int update(ClanskaKarta clanskaKarta);
	public int delete(ClanskaKarta clanskaKarta);
	public List<ClanskaKarta> find(String naziv, String trener, Integer cijenaOd, Integer cijenaDo, String vrstaTreninga, String nivoTreninga, Integer trejenjeOd, Integer trajanjeDo, String sortiranjeOcjena);

}
