package com.ognjenlazic.LazicGym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LazicGymApplication {

	public static void main(String[] args) {
		SpringApplication.run(LazicGymApplication.class, args);
	}

}
